(function ($) {
  Drupal.behaviors.wtSlick = {
    attach: function (context, settings) {
      $('[data-slick]', context).once('wtSlick').slick();
    }
  };
}(jQuery));
