(function ($) {
  var wt = window.wt || {};

  wt.initScrollpos = function () {
    ScrollPosStyler.init({
      scrollOffsetY: (function(){ return $('.wt__header').hasClass('wt__header--banner') ? $('.wt__header').height() : 0})(),
      spsClass: 'wt__header',
      classAbove: 'wt__header--scrollpos-above',
      classBelow: 'wt__header--scrollpos-below'
    });
  }
  wt.ready.push(wt.initScrollpos());
}(jQuery));
