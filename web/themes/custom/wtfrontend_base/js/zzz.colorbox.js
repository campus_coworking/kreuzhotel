(function ($) {
  var wt = window.wt || {};

  /**
   * The only reason for the behavior is to set up related galleries for prev/next links.
   * Otherwise delegated/direct colorbox calls would ignore the rel-setting (colorbox bug!)
   */ 
  Drupal.behaviors.wtColorbox = {
    attach: function (context, settings) {
      var options = {
        'maxWidth': '90%', 
        'maxHeight': '90%', 
        'width': function () {
          return (RegExp('^.+\.(png|jpeg|jpg|gif|webp|pdf)([\?#].*)?$', 'i').test( $(this).attr('href') ) ? false : '90%');
        },
        'height': function () {
          return (RegExp('^.+\.(png|jpeg|jpg|gif|webp|pdf)([\?#].*)?$', 'i').test( $(this).attr('href') ) ? false : '90%');
        },
        'iframe': function () {
          return !RegExp('^.+\.(png|jpeg|jpg|gif|webp|pdf)([\?#].*)?$', 'i').test( $(this).attr('href') );
        },
        'rel': function() { 
          return $(this).attr('data-colorbox-rel'); 
        }
      };
      $('[target="modal"]', context).once('wtColorbox').colorbox( $(this).attr('data-colorbox') ? $.extend( options, JSON.parse( $(this).attr('data-colorbox') ) ) : options );
    }
  };

  wt.addLightboxes = function() {
    $('.wt__frontend').on('click', '[target=modal]', function (ev) {
      try {
        dataLayer.push({
          'event': 'GAEvent',
          'eventCategory': 'lightbox',
          'eventAction': $(this).attr('data-ga-action'),
          'eventLabel': wt.findGALabel($(this)),
          'eventValue': $(this).attr('href')
        });
      }
      catch (err) {}
      if ( $(window).width() > wt.colorboxTreshold) {
        $(this).colorbox();
        ev.preventDefault();
      }
      else {
        window.open(this.href, '_blank');
        return false;
      }
    });
  };
  wt.ready.push( wt.addLightboxes );
}(jQuery));
