(function ($, Drupal) {
  var wt = window.wt || {};
  
  wt.adjustSysMsgContainer = function() {
    $sysMsgContainer = $('.row--sysmsg'); 
    if ($sysMsgContainer.find('.sysmsg__group').length >= 1) {
      $sysMsgContainer.removeClass('sysmsg--closed').addClass('sysmsg--open');
    }
    else {
      $sysMsgContainer.removeClass('sysmsg--open').addClass('sysmsg--closed');
    }
  }
  wt.ready.push( wt.adjustSysMsgContainer );
  $('.row--sysmsg').on('DOMSubtreeModified', wt.adjustSysMsgContainer );
  
  wt.addLocalTasksToToolbar = function () {
    $toolbar = $('#toolbar-bar');
    $listLink = $('#block-local-tasks a[data-drupal-link-system-path^="admin/wt_"]');
    if ($listLink.length == 1 && $toolbar.length == 1) {
      $toolbar.append('<div class="toolbar-tab"><a href="' + $listLink.attr('href') + '" class="toolbar-icon toolbar-icon-tablink-list toolbar-icon-tablink toolbar-item">' + $listLink.html() + '</a></div>');
    }
    $editLink = $('#block-local-tasks a[data-drupal-link-system-path^="node/"][data-drupal-link-system-path$="/edit"]');
    if ($editLink.length == 1 && $toolbar.length == 1) {
      $toolbar.append('<div class="toolbar-tab"><a href="' + $editLink.attr('href') + '" class="toolbar-icon toolbar-icon-tablink-edit toolbar-icon-tablink toolbar-item">' + $editLink.html() + '</a></div>');
    }
    $translateLink = $('#block-local-tasks a[data-drupal-link-system-path^="node/"][data-drupal-link-system-path$="/translations"]');
    if ($translateLink.length == 1 && $toolbar.length == 1) {
      $toolbar.append('<div class="toolbar-tab"><a href="' + $translateLink.attr('href') + '" class="toolbar-icon toolbar-icon-tablink-translate toolbar-icon-tablink toolbar-item">' + $translateLink.html() + '</a></div>');
    }
    $previewLink = $('.node-preview-container a.node-preview-backlink');
    if ($previewLink.length == 1 && $toolbar.length == 1) {
      $toolbar.append('<div class="toolbar-tab"><a href="' + $previewLink.attr('href') + '" class="toolbar-icon toolbar-icon-tablink-back toolbar-icon-tablink toolbar-item">' + $previewLink.html() + '</a></div>');
    }
  }
  wt.ready.push( wt.addLocalTasksToToolbar );

  wt.setMainMenuState = function () {
    if ($(window).width() > wt.mainMenu.treshold && wt.mainMenu.viewMode != 'lg' ) {
      $(wt.mainMenu.container).removeClass('is-collapsed').addClass('is-expanded').css('display', '');
      $(wt.mainMenu.toggleBtn).attr('aria-expanded', 'true');
      wt.mainMenu.viewMode = 'lg';
    }
    else if ($(window).width() <= wt.mainMenu.treshold && wt.mainMenu.viewMode != 'xs') {
      $(wt.mainMenu.container).removeClass('is-expanded').addClass('is-collapsed').css('display', '');;
      $(wt.mainMenu.toggleBtn).attr('aria-expanded', 'false');
      wt.mainMenu.viewMode = 'xs';
    }
  }
  wt.ready.push( wt.setMainMenuState );
  $(window).on('resize', wt.setMainMenuState );
  
  wt.openMenu = function ($ul) {
    $ul.addClass('is-unstable');
    $ul.siblings('.nav__a').addClass('is-unstable')
    if (wt.mainMenu.type == 'vertical' && $ul.hasClass('nav__ul--main-1') && $(window).width() > wt.mainMenu.treshold) {
      $ul.stop(true, true).animate(
        {
          'width': 'toggle'
        },
        {
          'complete': function() {
            $ul.removeClass('is-collapsed').removeClass('is-unstable').addClass('is-expanded');
            $ul.siblings('.nav__a').removeClass('is-unstable');
          }
        }
      );
    }
    else {
      $ul.stop(true, true).slideDown({
        'complete': function () {
          $ul.removeClass('is-collapsed').removeClass('is-unstable').addClass('is-expanded');
          $ul.siblings('.nav__a').removeClass('is-unstable');
        }
      });
    }
  }

  wt.closeMenu = function ($ul) {
    $ul.addClass('is-unstable');
    $ul.siblings('.nav__a').addClass('is-unstable');
    if (wt.mainMenu.type == 'vertical' && $ul.hasClass('nav__ul--main-1') && $(window).width() > wt.mainMenu.treshold) {
      $ul.stop(true, true).animate(
        {
          'width': 'toggle'
        },
        {
          'complete': function() {
            $ul.removeClass('is-expanded').removeClass('is-unstable').addClass('is-collapsed');
            $ul.siblings('.nav__a').removeClass('is-unstable');
          }
        });
    }
    else {
      $ul.stop(true, true).slideUp({
        'complete': function () {
          $ul.removeClass('is-expanded').removeClass('is-unstable').addClass('is-collapsed');
          $ul.siblings('.nav__a').removeClass('is-unstable');
        }
      });
    }
  }

  wt.setMainMenuExpandedStyles = function($ul) {
    if (!$ul.hasClass('nav__ul--main-1')) {
      return;
    }

    if (wt.mainMenu.type == 'vertical') {
      if ($(window).width() > wt.mainMenu.treshold) {
        $ul.css('padding-top', ($ul.closest('.nav__ul--main-0').position().top + wt.toolbarHeight) + 'px');
      }
      else {
        $ul.css('padding-top', '');
      }
    }
  }

  wt.setMainMenuCollapsedStyles = function($ul) {
    if (!$ul.hasClass('nav__ul--main-1')) {
      return;
    }
    // do dynamic styling here
  }

  wt.openMainMenuOnHover = function () {
    $('.nav__li--main-0.nav__li--parent').hoverIntent({
      over: function () {
        var $ul = $(this).children('.nav__ul--main-1');
        if ($(window).width() > wt.mainMenu.treshold && $ul.hasClass('is-collapsed') && !$ul.hasClass('is-unstable')) {
          wt.setMainMenuExpandedStyles($ul);
          wt.openMenu($ul);
        }
      },
      out: function () {
        var $ul = $(this).children('.nav__ul--main-1');
        if ($(window).width() > wt.mainMenu.treshold && $ul.hasClass('is-expanded') && !$ul.hasClass('is-unstable')) {
          wt.setMainMenuCollapsedStyles($ul);
          wt.closeMenu($ul);
        }
      },
      timeout: 300
    });
  }
  wt.ready.push( wt.openMainMenuOnHover );

  wt.addSubMenuToggler = function () {
    $('.nav__a[href$="#toggle"]').click( function(ev) {
      $ul = $(this).siblings('.nav__ul');
      if ($(this).hasClass('is-unstable')) {
        // ignore click
      }
      else if ($ul.hasClass('is-expanded')) {
        wt.setMainMenuCollapsedStyles($ul);
        wt.closeMenu($ul);
      }
      else if ($ul.hasClass('is-collapsed')) {
        $(this).closest('.nav__ul').find('.nav__ul.is-expanded').each( function() {
          wt.setMainMenuCollapsedStyles($(this));
          wt.closeMenu($(this));
        });
        wt.setMainMenuExpandedStyles($ul);
        wt.openMenu($ul);
      }
      ev.preventDefault();
      ev.stopPropagation();
    });
  }
  wt.ready.push( wt.addSubMenuToggler );


  wt.addVisibilityToggler = function () {
    $('.wt__frontend').on('click', '[data-toggle]', function (ev) {
      $source = $(this);
      $target = $($source.attr('data-target'));
      toggleMode = $source.attr('data-toggle');
      animationProperties = {};
      animationOptions = {};
      if (toggleMode == 'collapse') {
        functionNameToggleToExpand = 'slideDown';
        functionNameToggleToCollapse = 'slideUp';
      }
      else if (toggleMode == 'fade') {
        functionNameToggleToExpand = 'fadeIn';
        functionNameToggleToCollapse = 'fadeOut';
      }
      else if (toggleMode == 'flyout') {
        functionNameToggleToExpand = 'animate';
        functionNameToggleToCollapse = 'animate';
        animationProperties = {'width': 'toggle'};
      }
      else {
        console.error('Unknown data-toggle value. Use collapse (animate height), fade (animate opacity) or flyout (animate width).');
        return;
      }
      if ($source.attr('data-animation-duration') == parseInt($source.attr('data-animation-duration'), 10)) {
        animationOptions.duration = $source.attr('data-animation-duration');
      }
      else {
        animationOptions.duration = 400;
      }
      if (!$target.hasClass('is-unstable')) {
        if ($target.hasClass('is-expanded')) {
          animationOptions.complete = function() {
            $source.attr('aria-expanded', 'false');
            $target.removeClass('is-expanded').removeClass('is-unstable').addClass('is-collapsed');
          }

          $target.addClass('is-unstable');
          if (!$.isEmptyObject(animationProperties)) {
            $target[functionNameToggleToCollapse](animationProperties, animationOptions);
          }
          else {
            $target[functionNameToggleToCollapse](animationOptions);
          }
        }
        else {
          animationOptions.complete = function() {
            $source.attr('aria-expanded', 'true');
            $target.removeClass('is-collapsed').removeClass('is-unstable').addClass('is-expanded');
          }

          $target.addClass('is-unstable');
          $target[functionNameToggleToCollapse](animationOptions);
          if ($source.attr('data-toggle-to-display')) {
            // toggle to that specific display-property when fully visible
            // needed when initially toggling from "display: none" or
            // to a state other then the elements initial display property
            $target.css('display', $source.attr('data-toggle-to-display')).hide();
            // once done this is stored in jQuery's internal data store and no longer needed
            $source.removeAttr('data-toggle-to-display');
          }
          $target.addClass('is-unstable');
          if (!$.isEmptyObject(animationProperties)) {
            $target[functionNameToggleToExpand](animationProperties, animationOptions);
          }
          else {
            $target[functionNameToggleToExpand](animationOptions);
          }
        }
      }
      ev.preventDefault();
    });
  };
  wt.ready.push( wt.addVisibilityToggler );
  
  wt.scrollTo = function(uri) {
    if ((uri.indexOf(location.pathname) === 0 || uri.indexOf('#scroll') === 0) && location.pathname != '/' && uri.indexOf('#scroll') !== -1) {
      var selector = decodeURIComponent(uri.substring(uri.indexOf('#scroll') + '#scroll'.length));
      var regex = /^=[#\.\[\]a-zA-Z0-9-_]+$/;
      if (selector.search(regex) != -1) {
        selector = selector.substring(1);
        try {
          $('html, body').animate({scrollTop: $(selector).offset().top - wt.fixedTop }, 1000);
          dataLayer.push({
            'event' : 'GAEvent',
            'eventCategory' : 'scrollto',
            'eventAction' : selector,
            'eventLabel' : undefined,
            'eventNonInteraction': true
          });
        }
        catch (err) { /* fail silently */ }
      }
      return false;
    }
  }
  
  wt.addScrollTo = function () {
    $('.wt__frontend').on('click', 'a[href*="#scroll"]', function () {
      wt.scrollTo($(this).attr('href'));
    });
  }
  wt.ready.push( wt.addScrollTo );

  wt.imagesLoaded = function() {
    $('body').imagesLoaded( function() {
      window.setTimeout( wt.scrollTo(location.pathname + location.hash), 200 );
    });
  }
  wt.ready.push( wt.imagesLoaded );
  
  wt.findGALabel = function ($elem) {
    if ($elem.attr('data-ga-label')) {
      return $elem.attr('data-ga-label');
    }
    else if ($elem.parents('.wt__main').length > 0) {
      return 'main';
    }
    else if ($elem.parents('.wt__header').length > 0) {
      return 'header';
    }
    else if ($elem.parents('.wt__footer').length > 0) {
      return 'footer';
    }
    else if ($elem.parents('.wt__aside').length > 0) {
      return 'aside';
    }
    else {
      return undefined;
    }
  }
  wt.addGAEvents = function () {
    try {
      $('.wt__frontend').on('click', '[data-ga-event="GAEvent"]', function() {
        dataLayer.push({
          'event' : 'GAEvent',
          'eventCategory' : $(this).attr('data-ga-category') ? $(this).attr('data-ga-category') : 'custom',
          'eventAction' : $(this).attr('data-ga-action'),
          'eventLabel' : wt.findGALabel($(this)),
          'eventNonInteraction': false
        });
      });
      $('.wt__frontend').on('click', 'a[href^="http"]', function() {
        var href = $(this).attr('href');
        if (RegExp('^https?://').test(href) && !RegExp(location.host).test(href) && href.indexOf('whatsapp.com/send') == -1 && !RegExp('(\.pdf|\.zip|\.docx?\.xlsx?)$', 'i').test(href)) {
          dataLayer.push({
            'event' : 'GAEvent',
            'eventCategory' : $(this).attr('data-ga-category') ? $(this).attr('data-ga-category') : 'outbound',
            'eventAction' : href,
            'eventLabel' : wt.findGALabel($(this)),
            'eventNonInteraction': false
          });
        }
      });
      $('.wt__frontend').on('click', 'a[href$=".pdf"], a[href$=".zip"], a[href$=".doc"], a[href$=".docx"], a[href$=".xls"], a[href$=".xlsx"]', function() {
        dataLayer.push({
          'event' : 'GAEvent',
          'eventCategory' : $(this).attr('data-ga-category') ? $(this).attr('data-ga-category') : 'download',
          'eventAction' : $(this).attr('href'),
          'eventLabel' : wt.findGALabel($(this)),
          'eventNonInteraction': false
        });
      });
      $('.wt__frontend').on('click', 'a[href^="tel:"]', function() {
        dataLayer.push({
          'event' : 'GAEvent',
          'eventCategory' : $(this).attr('data-ga-category') ? $(this).attr('data-ga-category') : 'phone',
          'eventAction' : $(this).attr('href').substring('tel:'.length),
          'eventLabel' : wt.findGALabel($(this)),
          'eventNonInteraction': false
        });
      });
      $('.wt__frontend').on('click', 'a[href^="mailto:"]', function() {
        dataLayer.push({
          'event' : 'GAEvent',
          'eventCategory' : $(this).attr('data-ga-category') ? $(this).attr('data-ga-category') : 'email',
          'eventAction' : $(this).attr('href').substring('mailto:'.length),
          'eventLabel' : wt.findGALabel($(this)),
          'eventNonInteraction': false
        });
      });
      $('.wt__frontend').on('click', 'a[href^="https://api.whatsapp.com/send?phone="]', function () {
        dataLayer.push({
          'event' : 'GAEvent',
          'eventCategory' : $(this).attr('data-ga-category') ? $(this).attr('data-ga-category') : 'whatsapp',
          'eventAction' : $(this).attr('href').substring('https://api.whatsapp.com/send?phone='.length),
          'eventLabel' : wt.findGALabel($(this)),
          'eventNonInteraction': false
        });
        if (navigator.platform.indexOf('Mac') > -1 || navigator.platform.indexOf('Win') > -1) {
          window.open(this.href.replace('https://api.whatsapp.com/', 'https://web.whatsapp.com/'));
          return false;
        }
      });
    }
    catch (err) { /* fail silently */ }
  }
  wt.ready.push( wt.addGAEvents );
  
  wt.addScrollDepthEvents = function() {
    if (typeof(dataLayer) !== 'undefined') {
      jQuery.scrollDepth({
        elements: ['.wt__main', '.wt__footer'],
        percentage: true,
        pixelDepth: false,
        userTiming: false,
        eventHandler: function(data) {
          data.eventCategry = data.event;
          data.event = 'GAEvent';
          dataLayer.push(data);
        }
      });
    }
  }
  wt.ready.push( wt.addScrollDepthEvents );

  wt.gaOptout = function () {
    $('.wt__frontend').on('click', 'a[href*="#optout"]', function () {
      gaOptout();
      alert(Drupal.t('Google Analytics tracking disabled.'));
    });
  }
  wt.ready.push( wt.gaOptout );

  wt.cookie = {};
  wt.cookie.showConsentMessage = function () {
    if (!Cookies.get('wt.cookieConsent')) {
      $('#block-cookieconsent').fadeIn();
    }
  };
  wt.cookie.storeConsent = function () {
    $('.wt__frontend').on('click', 'a[href*="#cookieConsent"]', function () {
      Cookies.set('wt.cookieConsent', true, { expires: 365 });
      $('#block-cookieconsent').fadeOut();
      return false;
    });
  }
  wt.ready.push( wt.cookie.showConsentMessage );
  wt.ready.push( wt.cookie.storeConsent );

}(jQuery, Drupal));
