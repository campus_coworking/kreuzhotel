(function ($) {
  var wt = window.wt || {};
  wt.map = {};
  
  wt.map.getCurrentPosition = function(mapId) {
    var deferred = $.Deferred();
    navigator.geolocation.getCurrentPosition(
      function(position) {
        drupalSettings.geolocation.maps[mapId].wtCurrentPosition = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        deferred.resolve();
      },
      function() {
        deferred.reject();
      },
      { timeout: 5000 }
    );
    return deferred.promise();
  }
  
  wt.map.getRoute = function (start, mapId, paragraphId) {
    $('.map__message.is-expanded button').click();

    var panelId = 'map__directions--' + paragraphId;
    var request = {
      origin: start,
      destination: drupalSettings.geolocation.maps[mapId].mapMarkers[0].internalPosition,
      travelMode: google.maps.DirectionsTravelMode.DRIVING
    };
    drupalSettings.geolocation.maps[mapId].wtDirectionsService = drupalSettings.geolocation.maps[mapId].wtDirectionsService || new google.maps.DirectionsService();
    drupalSettings.geolocation.maps[mapId].wtDirectionsDisplay = drupalSettings.geolocation.maps[mapId].wtDirectionsDisplay || new google.maps.DirectionsRenderer();
    drupalSettings.geolocation.maps[mapId].wtDirectionsDisplay.setMap( drupalSettings.geolocation.maps[mapId].googleMap );
    drupalSettings.geolocation.maps[mapId].wtDirectionsDisplay.setPanel( document.getElementById(panelId) );
    
    drupalSettings.geolocation.maps[mapId].wtDirectionsService.route(request, function(response, status) {
      if (status == google.maps.DirectionsStatus.OK) {
        drupalSettings.geolocation.maps[mapId].wtDirectionsDisplay.setDirections(response);
        $('#' + panelId).slideDown();
        if (typeof(start) == 'object') {
          $('#map__input--' + paragraphId).val(response.routes[0].legs[0].start_address);
        }
      } else if (status == google.maps.DirectionsStatus.NOT_FOUND) {
        $('#map__message--NOTFOUND--' + paragraphId + '.is-collapsed button').click();
      } else {
        $('#map__message--UNKNOWN--' + paragraphId + '.is-collapsed button').click();
      }
    });
    return false;
  }

  Drupal.behaviors.wtMapDirectionsFromInput = {
    attach: function (context, settings) {
      $('.map__form', context).once('wtMapDirectionsFromInput').on( 'submit', function() {
        var mapId = $(this).parents('.paragraph--map').find('.geolocation-formatter-map-wrapper').attr('id');
        var paragraphId = $(this).attr('data-paragraph');
        wt.map.getRoute( $(this).find('.map__input').val(), mapId, paragraphId );
        return false;
      });
    }
  };
  
  Drupal.behaviors.wtMapDirectionsFromCurrentLoc = {
    attach: function (context, settings) {
      $('.map__btn--geo', context).once('wtMapDirectionsFromCurrentLoc').on( 'click', function() {
        var mapId = $(this).parents('.paragraph--map').find('.geolocation-formatter-map-wrapper').attr('id');
        var paragraphId = $(this).attr('data-paragraph');
        $.when( wt.map.getCurrentPosition(mapId) ).done( function() {
          wt.map.getRoute( drupalSettings.geolocation.maps[mapId].wtCurrentPosition, mapId, paragraphId );
        }).fail( function() {
          $(this).parents('.map__form').find('.map__message--MYPOSITION.is-collapsed button').click();
          drupalSettings.geolocation.maps[mapId].wtCurrentPosition = false;
        })
      });
    }
  };
}(jQuery));
