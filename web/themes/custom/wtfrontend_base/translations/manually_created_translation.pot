# $Id$
#
# LANGUAGE translation of Webtourismus Drupal themes and modules
# Copyright YEAR NAME <EMAIL@ADDRESS>
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2018-05-09 13:23+0200\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"


msgid "Media library"
msgstr ""

msgid "Create new image"
msgstr ""

msgid "Create new video"
msgstr ""

msgid "Create new document"
msgstr ""

msgid "Create new media"
msgstr ""

msgid "Manage media categories"
msgstr ""

msgid "Navigation menu"
msgstr ""

msgid "Navigation menus"
msgstr ""

msgid "Global block"
msgstr ""

msgid "Global blocks"
msgstr ""

msgid "Category"
msgstr ""

msgid "Categories"
msgstr ""

msgid "Price"
msgstr ""

msgid "Prices"
msgstr ""

msgid "Availability"
msgstr ""

msgid "Availabilities"
msgstr ""

msgid "Check availability"
msgstr ""

# accomodation
msgid "Prices & Availabilities"
msgstr ""

msgid "Loading prices and availabilities..."
msgstr ""

# accomodation
msgid "Available"
msgstr ""

# accomodation
msgid "Unavailable"
msgstr ""

# accomodation
msgid "Only departure"
msgstr ""

# accomodation
msgid "Only arrival"
msgstr ""

# accomodation
msgid "No arrival or departure"
msgstr ""

# room amenities 
msgid "Amenities"
msgstr ""

# verb
msgid "enquire"
msgstr ""

# verb
msgid "book"
msgstr ""

# verb
msgid "book online"
msgstr ""

# verb but capitalize only first word
msgid "Book online"
msgstr ""

# noun
msgid "Booking"
msgstr ""

# noun
msgid "Online booking"
msgstr ""

# noun singular
msgid "Enquiry"
msgstr ""

# noun plural
msgid "Enquiries"
msgstr ""

# noun plural
msgid "Gift card"
msgstr ""

msgid "Gift cards"
msgstr ""

msgid "To the frontpage"
msgstr ""

msgid "To the blog"
msgstr ""

# return to top of a long web page
msgid "to top"
msgstr ""

msgid "All pages"
msgstr ""

msgid "Create new page"
msgstr ""

# holiday packages
msgid "All packages"
msgstr ""

msgid "All offers"
msgstr ""

msgid "All events"
msgstr ""

msgid "All rooms"
msgstr ""

msgid "All news"
msgstr ""

# point of interest
msgid "All POIs"
msgstr ""

# subscribe newsletter
msgid "subscribe"
msgstr ""

# unsubscribe newsletter
msgid "unsubscribe"
msgstr ""

# submit a web form
msgid "submit"
msgstr ""

# submit a web form
msgid "send"
msgstr ""

msgid "Read more"
msgstr ""

msgid "more"
msgstr ""

msgid "Details"
msgstr ""

msgid "Arrival"
msgstr ""

msgid "Check-in"
msgstr ""

msgid "Departure"
msgstr ""

msgid "Check-out"
msgstr ""

msgid "Person"
msgstr ""

msgid "Persons"
msgstr ""

msgid "Adult"
msgstr ""

msgid "Adults"
msgstr ""

msgid "Child"
msgstr ""

msgid "Children"
msgstr ""

msgid "Age of children"
msgstr ""

msgid "Number of rooms"
msgstr ""

# number of (...products)
msgid "Quantity"
msgstr ""

# short for number of (...products)
msgid "qty"
msgstr ""

msgid "Room type"
msgstr ""

msgid "Accomodation type"
msgstr ""

msgid "Meal plan"
msgstr ""

msgid "Best price guaranteed!"
msgstr ""

# e.g. 12 o'clock
msgid "o'clock"
msgstr ""

# personalized ad tracking in web browsers
msgid "Google Analytics tracking disabled."
msgstr ""

# minimum length of stay for a holiday package, e.g. 3 or more nights (do not translate @COUNT )
msgid "from @COUNT night"
msgid_plural "from @COUNT nights"
msgstr[0] ""
msgstr[1] ""

# minimum length of stay for a holiday package, e.g. 3 nights (do not translate @COUNT )
msgid "@COUNT night"
msgid_plural "@COUNT nights"
msgstr[0] ""
msgstr[1] ""

# minimum length of stay for a holiday package, e.g. 3 or more days (do not translate @COUNT )
msgid "from @COUNT day"
msgid_plural "from @COUNT days"
msgstr[0] ""
msgstr[1] ""

# minimum length of stay for a holiday package, e.g. 3 days (do not translate @COUNT )
msgid "@COUNT day"
msgid_plural "@COUNT days"
msgstr[0] ""
msgstr[1] ""

msgid "Address not recognized"
msgstr ""

msgid "Unkown error"
msgstr ""

msgid "Unable to detect current location"
msgstr ""

msgid "Get directions from my current location"
msgstr ""

# start address to get directions 
msgid "Enter a location"
msgstr ""

# Get directions to travel by car 
msgid "Get directions"
msgstr ""

msgid "Print directions"
msgstr ""

# date format 31.12.2000 
msgid "DD.MM.YYYY"
msgstr ""

# verb 
msgid "search"
msgstr ""

# noun 
msgid "Search"
msgstr ""

# verb 
msgid "filter"
msgstr ""

# noun 
msgid "Filter"
msgstr ""

msgid "apply"
msgstr ""

# on a web page 
msgid "log in"
msgstr ""

# at least of one of a set of choices
msgid "- Any -"
msgstr ""

msgid "day"
msgstr ""

msgid "days"
msgstr ""

msgid "night"
msgstr ""

msgid "nights"
msgstr ""

# price...
msgid "per day"
msgstr ""

# price...
msgid "per night"
msgstr ""

# price...
msgid "per person"
msgstr ""

# price starting at...
msgid "from"
msgstr ""

msgid "My data"
msgstr ""

msgid "Travel plan"
msgstr ""

msgid "I'm interested in"
msgstr ""

msgid "Forgot your password?"
msgstr ""
