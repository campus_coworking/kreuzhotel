(function ($, Drupal) {
  var wt = window.wt || {};

  /*
   * =====
   * do this to remove/override an "on ready" function defined in wtfrontend_base theme
   * =====

  wt.ready = wt.ready.filter( function(removeHandler) {return removeHandler != wt.oldSomething});
  wt.newSomething = function () {
    // do something new instead of something old from wtfrontend_base
  }
  wt.ready.push( wt.newSomething );

   * =====
   */


  /*
   * =====
   * add your own, new functions here
   * =====

  wt.myCustomFunc = function () { ... }
  wt.ready.push( wt.myCustomFunc );

   * =====
   * or
   * =====

  Drupal.behaviors.wtCustomBehavior = {
    attach: function (context, settings) {
      ...
    }
  }

   * =====
   */

  // this function needs to solve two problems:
  // 1. if the design has a fixed top element (e.g. a fixed navbar),
  //    it needs to correctly set the wt.fixedTop variable
  // 2. if the Drupal Admin Toolbar is visible (e.g. when visitor is logged in
  //    as editor), it needs to add a body { padding-top: XX } to offset
  //    the header and prevent clashes between Drupal Admin Toolbar and Header
  //    (Drupal Admin Toolbar must push down the entire content)
  wt.adjustFrontendToToolbar = function () {
    var defer = $.Deferred();
    window.setTimeout( function() {
      var toolbarBarHeight = $('#toolbar-bar').length>0 ? toolbarBarHeight = $('#toolbar-bar').height() : 0;
      var toolbarTrayHeight = $('.toolbar-fixed.toolbar-tray-open .toolbar-tray-horizontal').length>0 ? $('.toolbar-fixed.toolbar-tray-open .toolbar-tray-horizontal').height() : 0 ;
      var toolbarTrayWidth = $('.toolbar-fixed.toolbar-tray-open .toolbar-tray-vertical').length>0 ? $('.toolbar-fixed.toolbar-tray-open .toolbar-tray-vertical').width() : 0 ;
      wt.toolbarHeight = toolbarBarHeight + toolbarTrayHeight;
      $('body').css('paddingTop', wt.toolbarHeight + 'px');
      $('.region--header').css('top', wt.toolbarHeight + 'px');
      wt.fixedTop = wt.toolbarHeight;
      defer.resolve();
    }, 50);
    return defer.promise();
  }
  $(document).on('drupalToolbarOrientationChange drupalToolbarTrayChange', wt.adjustFrontendToToolbar );
  wt.ready.push( wt.adjustFrontendToToolbar );


  wt.ready.forEach( function(executeHandler) {
    $(executeHandler);
  });

}(jQuery, Drupal));





