<?php 

$settings['trusted_host_patterns'][] = '^.+\.FIXXXME---ONE-OR-MORE_LIVEDOMAINS\.at$';

$databases['default']['default'] = array (
  'database' => 'FIXXXME',
  'username' => 'FIXXXME',
  'password' => 'FIXXXME',
  'prefix' => '',
  'host' => 'localhost',
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);
