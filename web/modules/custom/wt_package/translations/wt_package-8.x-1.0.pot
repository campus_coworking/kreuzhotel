# $Id$
#
# LANGUAGE translation of Drupal (general)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  modules/custom/wt_package/wt_package.links.action.yml: n/a
#  modules/custom/wt_package/wt_package.links.task.yml: n/a
#  modules/custom/wt_package/wt_package.links.menu.yml: n/a
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2018-05-09 13:07+0200\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: modules/custom/wt_package/wt_package.links.action.yml:0
msgid "Create new package"
msgstr ""

#: modules/custom/wt_package/wt_package.links.task.yml:0
msgid "All packages"
msgstr ""

#: modules/custom/wt_package/wt_package.links.menu.yml:0
msgid "Packages"
msgstr ""

#: modules/custom/wt_package/wt_package.links.menu.yml:0
msgid "Edit and create packages"
msgstr ""

