<?php

namespace Drupal\wt_ctalinks;
use Drupal\node\NodeInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\config_pages\Entity\ConfigPages;
use Drupal\node\Entity\Node;


/**
 * Class DefaultService.
 */
class CtaLinksPathModifierService implements OutboundPathProcessorInterface {

  /**
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  protected $bookingPath;
  protected $enquirePath;

  /**
   * Constructs a new DefaultService object.
   */
  public function __construct(RequestStack $request_stack) {
    $this->requestStack = $request_stack;
    $this->bookingPath = NULL;
    $this->enquiryPath = NULL;
    if ($projectSettings = ConfigPages::config('project')) {
      if ($projectSettings->hasField('field_bookingpage') && ($erf = $projectSettings->field_bookingpage->first())) {
        $this->bookingPath = '/node/' . $erf->target_id;
      }
      if ($projectSettings->hasField('field_enquirepage') && ($erf = $projectSettings->field_enquirepage->first())) {
        $this->enquirePath = '/node/' . $erf->target_id;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = array(), Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL) {
    if (empty($request)) {
      $request = $this->requestStack->getCurrentRequest();
    }

    if (array_key_exists('wt_ctalinks_nid', $options) && $options['wt_ctalinks_nid']) {
      $node = Node::load($options['wt_ctalinks_nid']);
      unset($options['wt_ctalinks_nid']);
    } else {
      $node = $request->attributes->get('node');
    }

    if (is_numeric($node)) {
      // missing upcasting in revision view, have to load manually there
      $node = Node::load($node);
    }

    if ($node instanceof NodeInterface && ($node->getType() === 'wt_room' || $node->bundle() === 'wt_package')) {
      if (strpos($path, $this->bookingPath) !== FALSE || strpos($path, $this->enquirePath) !== FALSE) {
        $options['query'][$node->bundle()] = $node->id();

        if ($node->hasField('remote_datasource') && $node->hasField('remote_id')) {
          if (($remoteSource = $node->remote_datasource->first()->value) && ($remoteId = $node->remote_id->first()->value)) {
            switch ($remoteSource) {
              case 'CASABLANCA':
                if ($node->bundle() == 'wt_room') {
                  $options['query']['casaroomtype'] = $remoteId;
                }
                elseif ($node->bundle() == 'wt_package') {
                  $options['query']['casapackage'] = $remoteId;
                }
                break;
              case 'KOGNITIV':
                if ($node->bundle() == 'wt_room') {
                  $options['fragment'] = '!/skd-ds/skd-room-view/' . $remoteId;
                }
                elseif ($node->bundle() == 'wt_package') {
                  $options['fragment'] = '!/skd-ds/skd-package-view/' . $remoteId;
                }
                if (array_key_exists('wt_ctalinks_fragment_postfix', $options)) {
                  $options['fragment'] .= $options['wt_ctalinks_fragment_postfix'];
                  unset($options['wt_ctalinks_fragment_postfix']);
                }
                break;
              case 'PROTEL':
              case 'FERATEL':
              default:
                if ($node->bundle() == 'wt_room') {
                  $options['query']['remote_room'] = $remoteId;
                }
                elseif ($node->bundle() == 'wt_package') {
                  $options['query']['remote_package'] = $remoteId;
                }
                break;
            }
          }
        }
      }
    }
    if ($bubbleable_metadata) {
      $bubbleable_metadata
        ->addCacheContexts([
          'wt_ctalinks',
        ]);
    }
    return $path;
  }
}