<?php

namespace Drupal\wt_base\Controller;

use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Access\AccessResult;

class AdminController extends ControllerBase {
    
  const ROUTENAME_REDIRECT_ADMINLIST = ':BUNDLE_NAME.admin_list';
  const ROUTENAME_VIEWS_ADMINLIST = 'view.:BUNDLE_NAME.admin_list';

  /**
   * 
   * The only purpose of this controller is to provide an
   * alternative path to the correspondig admin list view, 
   * so we can attach a _custom_access check to it.
   *  
   */
  public function redirectToAdminList(Request $request, $bundle_name) {
    return $this->redirect(strtr(self::ROUTENAME_VIEWS_ADMINLIST, [':BUNDLE_NAME' => $bundle_name]));
  }

  /**
   * 
   * Used to limit visibility of routes/local task "List" to their corresponding bundles.
   * Access to the forwarding/filtering route itself is always allowed, 
   * the real access check must be done by the correspondig views page target!
   *  
   */
  public function isAllowedAdminList($bundle_name) {
    $currentlyViewedBundle = false;
    $currentRoute = \Drupal::routeMatch();
    $currentRouteName = $currentRoute->getRouteName();
    $node = $currentRoute->getParameter('node');
    if ($node instanceof \Drupal\node\NodeInterface) {
      $currentlyViewedBundle = $node->bundle();
    }
    return AccessResult::allowedIf(
      $bundle_name && (
        $currentlyViewedBundle === $bundle_name || 
        $currentRouteName === strtr(self::ROUTENAME_REDIRECT_ADMINLIST, [':BUNDLE_NAME' => $bundle_name])
      )
    );
  }
}
