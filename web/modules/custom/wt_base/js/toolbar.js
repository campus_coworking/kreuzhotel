(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.wtcoremodSetToolbarDefaults = {
    attach: function (context, settings) {
      if (Drupal.toolbar.views.toolbarVisualView) {
        if (!localStorage.getItem('Drupal.toolbar.subtreesHash.wtadmin') && !localStorage.getItem('Drupal.toolbar.subtreesHash.wtfrontend')) {
          localStorage.setItem('Drupal.toolbar.trayVerticalLocked', 'true');
          Drupal.toolbar.views.toolbarVisualView.model.set({locked: true, orientation: 'vertical'}, {validate: true, override: true});
        }
        if ( drupalSettings.path.currentPathIsAdmin && 
             Drupal.toolbar.views.toolbarVisualView.model.attributes.isFixed && 
             localStorage.getItem('Drupal.toolbar.activeTabID') != '"toolbar-item-administration"'
        ) {
          Drupal.toolbar.views.toolbarVisualView.model.set({activeTab: $('#toolbar-item-toolbar-menu-editor')});
        }
        else if (!drupalSettings.path.currentPathIsAdmin) {
          Drupal.toolbar.views.toolbarVisualView.model.set({activeTab: null});
        }
      }
    }
  };
})(jQuery, Drupal, drupalSettings);
