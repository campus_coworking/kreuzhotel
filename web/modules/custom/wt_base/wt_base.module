<?php

use Drupal\Core\Form\FormStateInterface;


function wt_base_preprocess_html(array &$variables) {
  if (strpos($_SERVER['HTTP_HOST'], 'entwicklung.webtourismus.at') !== false) {
    $variables['attributes']['class'][] = 'env--dev';
  }
  else {
    $variables['attributes']['class'][] = 'env--prod';
  }

  $user = \Drupal::currentUser();
  if ($user->hasPermission('access toolbar')) {
    $variables['#attached']['library'][] = 'wt_base/toolbar';
  }
}

/**
 * we need the node object in page.html.twig to preview banner slides
 */

function wt_base_preprocess_page(array &$variables) {
  $route = \Drupal::routeMatch();
  if ($route->getRouteName() == 'entity.node.preview') {
    $node = $route->getParameter('node_preview');
    if ($node) {
      $variables['node'] = $node;
    }
  }
}

function wt_base_form_user_login_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $form['#submit'][] = '_wt_base_user_login_form_submit';
}



/**
 * Custom submit handler for login form.
 */
function _wt_base_user_login_form_submit($form, FormStateInterface $form_state) {
  // Set redirect to login form.
  $form_state->setRedirect('wt_page.admin_list');
}


/**
 * 
 * filter some text select list options in the node edit forms
 * 
 */
function wt_base_options_list_alter(array &$options, array $context) {
  $entity = $context['entity'];

  /**
   * field_bem_modifier is shared accross multiple paragraphs, but only modifier values
   * starting with the same name as the paragraph are valid modifiers
   */
  if (preg_match('/^paragraph\..+\.field_bem_modifier$/', $context['fieldDefinition']->id()) === 1) {
    $allowedPrefix = $entity->bundle() . '--';
    foreach ($options as $key => $value) {
      if (!(strpos($key, $allowedPrefix) === 0) && $key != '_none') {
        unset($options[$key]);
      }
    }
  }

  /**
   * some field_contentwidth_* values will only work with specific bundles
   */
  if (preg_match('/^paragraph\..+\.field_contentwidth_/', $context['fieldDefinition']->id()) === 1) {
    $allowedBundles = [];
    foreach ($options as $key => $value) {
      // field_contentwidth_* value *-auto is only meaningful for text
      $allowedBundles = ['text'];
      if (substr($key, -5, 5) === '-auto') {
        $allowedBundles = ['text'];
        if (!in_array($entity->bundle(), $allowedBundles)) {
          unset($options[$key]);
        }
      }
      else if (substr($key, -5, 5) === 'shrink') {
        // field_contentwidth_* value *-shrink is only meaningful for code, text and images
        $allowedBundles = ['code', 'text', 'image'];
        if (!in_array($entity->bundle(), $allowedBundles)) {
          unset($options[$key]);
        }
      }
    }
  }
}

// Add class to the user tab so it can be moved to the right.
function wt_base_toolbar_alter(&$items) {
  $user = \Drupal::currentUser();
  if ($user->hasPermission('access toolbar')) {
    $items['user']['#wrapper_attributes']['class'] = array('toolbar-tab--user');
    
    $roles = $user->getRoles();
    if (!in_array('administrator', $roles)) {
      unset($items['administration']);
    }
  }
}


function wt_base_form_node_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
  if (isset($form['body']['widget'][0]['summary']['#attributes'])) {
    unset($form['body']['widget'][0]['summary']['#attributes']);
  }
}



function wt_base_field_widget_paragraphs_form_alter(&$element, \Drupal\Core\Form\FormStateInterface $form_state, $context) {
  /** @var \Drupal\field\Entity\FieldConfig $field_definition */
  $fieldDefinition = $context['items']->getFieldDefinition();
  $parentEntityType = $fieldDefinition->getTargetEntityTypeId();
  $parentEntityBundle = $fieldDefinition->getTargetBundle();
  $parentEntityFieldType = $fieldDefinition->getType();
  $parentEntityFieldName = $fieldDefinition->getName();
  
  
  /**
   * 
   * Accordion paragraphs 
   *
   * Entries in an accordion block always need title and content, we use summary and body of a text field for it
   */
  if (isset($element['subform']['field_accordion']['widget'][0]['summary']['#attributes'])) {
    foreach ($element['subform']['field_accordion']['widget'] as $idx => $widget) {
      if (isset($element['subform']['field_accordion']['widget'][$idx]['summary']['#attributes'])) {
        unset($element['subform']['field_accordion']['widget'][$idx]['summary']['#attributes']);
        unset($element['subform']['field_accordion']['widget'][$idx]['summary']['#description']);
        $element['subform']['field_accordion']['widget'][$idx]['summary']['#title'] = t('Title') . ' ' .($idx + 1);;
        $element['subform']['field_accordion']['widget'][0]['summary']['#required'] = TRUE;
        $element['subform']['field_accordion']['widget'][$idx]['#title'] = t('Content') . ' ' .($idx + 1);
        $element['subform']['field_accordion']['widget'][$idx]['#title_display'] = 'before';
      }
    }
  } 

  /**
   *
   * Row paragraphs 
   *
   * Conditionally hide background image and parallax depending on row colorschema
   * 
   * http://agaric.com/blogs/conditional-fields-paragraphs-using-javascript-states-api-drupal-8
   */
  if ($parentEntityFieldName == 'field_pagebuilder') {
    
    /** @see \Drupal\paragraphs\Plugin\Field\FieldWidget\ParagraphsWidget::formElement() */
    $widgetState = \Drupal\Core\Field\WidgetBase::getWidgetState($element['#field_parents'], $parentEntityFieldName, $form_state);
    
    /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
    $paragraph = $widgetState['paragraphs'][$element['#delta']]['entity'];
    $paragraphBundle = $paragraph->bundle();
 
    // Determine which paragraph type is being embedded.
    if ($paragraphBundle == 'row') {
      $dependeeFieldName = 'field_colorschema';
      $selector = sprintf('select[name="%s[%d][subform][%s]"]', $parentEntityFieldName, $element['#delta'], $dependeeFieldName);
 
      // Dependent fields.
      $element['subform']['field_image']['#states'] = [
        'visible' => [
          [
            $selector => ['value' => '--image-bglight'],
          ],
          [
            $selector => ['value' => '--image-bgdark'],
          ],
        ],
        'required' => [
          [
            $selector => ['value' => '--image-bglight'],
          ],
          [
            $selector => ['value' => '--image-bgdark'],
          ],
        ],
      ];
      $element['subform']['field_parallax']['#states'] = [
        'visible' => [
          [
            $selector => ['value' => '--image-bglight'],
          ],
          [
            $selector => ['value' => '--image-bgdark'],
          ],
        ],
      ];
    }
  }
}