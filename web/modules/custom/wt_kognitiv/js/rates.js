(function ($) {
  var wtKognitiv = window.wtKognitiv || {};
  wtKognitiv.rate = wtKognitiv.rate || {};
  Drupal.behaviors.kognitivRateRoom = {
    attach: function (context, settings) {
      $('.wt__frontend').once('kognitiv_enquire_dates').on('click', '.url--enquire', function (ev) {
        if (wtKognitiv.rate.arrival) {
          $(this).attr('href', $(this).attr('href') + '&arrival=' + wtKognitiv.rate.arrival);
        }
        if (wtKognitiv.rate.departure) {
          $(this).attr('href', $(this).attr('href') + '&departure=' + wtKognitiv.rate.departure);
        }
      });
      $('.wt__frontend').once('kognitiv_book_dates').on('click', '.url--book', function (ev) {
        if (wtKognitiv.rate.arrival) {
          $(this).attr('href', $(this).attr('href').replace('#!/skd-ds/', '&skd-checkin=' + wtKognitiv.rate.arrival + '#!/skd-ds/'));
        }
        if (wtKognitiv.rate.departure) {
          $(this).attr('href', $(this).attr('href').replace('#!/skd-ds/', '&skd-checkout=' + wtKognitiv.rate.departure + '#!/skd-ds/'));
        }
        if (wtKognitiv.rate.arrival || wtKognitiv.rate.departure) {
          $(this).attr('href', $(this).attr('href').replace('#!/skd-ds/skd-room-view/', '#!/skd-ds/skd-room/').replace('#!/skd-ds/skd-package-view/', '#!/skd-ds/skd-package/'));
        }
      });
      /**
       * Room rates 
       */
      $('[data-kog-rates="room"]', context).once('kognitiv_rate_room').each( function () {
        var $ajaxContainer = $(this);
        $.ajax({
          url: $ajaxContainer.attr('data-kog-rates-callback'),
          data: {'nid': $(this).attr('data-kog-rates-node')},
          dataType: 'json',
          async: true,
          cache: false,
          success: function (result) {
            $ajaxContainer.replaceWith(result.html);
            var calStart = $('[data-kog-calendar]').attr('data-kog-calendar-startdate');
            var calLength = $('[data-kog-calendar]').attr('data-kog-calendar-length');
            /**
             * Room rates overview calendar 
             */
            $('[data-kog-calendar="roomtotal"]').datepicker({
              minDate: new Date(calStart),
              maxDate: '+' + calLength + 'D',
              dateFormat: 'yy-mm-dd',
              numberOfMonths: $(window).width() >= 540 ? 2 : 1,
              onSelect: function(date) {
                if (!wtKognitiv.rate.arrival || (wtKognitiv.rate.arrival && wtKognitiv.rate.departure)) {
                  wtKognitiv.rate.arrival = date;
                  delete wtKognitiv.rate.departure;
                }
                else if (wtKognitiv.rate.arrival && date > wtKognitiv.rate.arrival) {
                  wtKognitiv.rate.departure = date;
                }
                else if (wtKognitiv.rate.arrival && date < wtKognitiv.rate.arrival) {
                  wtKognitiv.rate.departure = wtKognitiv.rate.arrival;
                  wtKognitiv.rate.arrival = date;
                }
                else if (wtKognitiv.rate.arrival == date) {
                  delete wtKognitiv.rate.arrival;
                  delete wtKognitiv.rate.departure;
                }
                $('[data-kog-calendar="roomdetail"]').datepicker('refresh');
              },
              beforeShowDay: function (date) {
                var dd = ('0' + date.getDate()).slice(-2);
                var mm = ('0' + (date.getMonth()+1)).slice(-2);
                var yyyy = date.getFullYear();
                var dateString = yyyy + '-' + mm + '-' + dd;
                var css = 'day';
                var tooltip = false;

                if (dateString == wtKognitiv.rate.arrival) {
                  css += ' day__selected day__selected--start';
                }
                else if (dateString == wtKognitiv.rate.departure) {
                  css += ' day__selected day__selected--end';
                }
                else if (dateString > wtKognitiv.rate.arrival && dateString < wtKognitiv.rate.departure) {
                  css += ' day__selected day__selected--middle';
                }

                if ($('[data-kog-date="' + dateString + '"][data-kog-available="1"]').length > 0) {
                  css += ' day--available';
                  tooltip = Drupal.t('Available');
                }
                else {
                  css += ' day--unavailable';
                  tooltip = Drupal.t('Unavailable');
                }
                var $changeOvers = $('[data-kog-date="' + dateString + '"][data-kog-changeover]');
                if ($changeOvers.length > 0) {
                  var changeOverValues = $changeOvers.map( function () { 
                    return $(this).attr("data-kog-changeover");
                  }).get();
                  css += ' day--changeover-' + Math.min.apply(Math,changeOverValues);
                }
                return [true, css, tooltip]
              }
            });
            /**
             * Room rates detail calendar 
             */
            $('[data-kog-calendar="roomdetail"]').datepicker({
              minDate: new Date(calStart),
              maxDate: '+' + calLength + 'D',
              dateFormat: 'yy-mm-dd',
              numberOfMonths: $(window).width() >= 540 ? 2 : 1,
              onSelect: function(date) {
                if (!wtKognitiv.rate.arrival || (wtKognitiv.rate.arrival && wtKognitiv.rate.departure)) {
                  wtKognitiv.rate.arrival = date;
                  delete wtKognitiv.rate.departure;
                }
                else if (wtKognitiv.rate.arrival && date > wtKognitiv.rate.arrival) {
                  wtKognitiv.rate.departure = date;
                }
                else if (wtKognitiv.rate.arrival && date < wtKognitiv.rate.arrival) {
                  wtKognitiv.rate.departure = wtKognitiv.rate.arrival;
                  wtKognitiv.rate.arrival = date;
                }
                else if (wtKognitiv.rate.arrival == date) {
                  delete wtKognitiv.rate.arrival;
                  delete wtKognitiv.rate.departure;
                }
                var rate = $(this).attr('data-kog-calendar-rate');
                $('[data-kog-calendar]:not([data-kog-calendar-rate="' + rate + '"])').datepicker('refresh');
              },
              beforeShowDay: function (date) {
                var rate = $(this).attr('data-kog-calendar-rate');
                var dd = ('0' + date.getDate()).slice(-2);
                var mm = ('0' + (date.getMonth()+1)).slice(-2);
                var yyyy = date.getFullYear();
                var dateString = yyyy + '-' + mm + '-' + dd;
                var css = 'day';
                var tooltip = false;

                if (dateString == wtKognitiv.rate.arrival) {
                  css += ' day__selected day__selected--start';
                }
                else if (dateString == wtKognitiv.rate.departure) {
                  css += ' day__selected day__selected--end';
                }
                else if (dateString > wtKognitiv.rate.arrival && dateString < wtKognitiv.rate.departure) {
                  css += ' day__selected day__selected--middle';
                }

                var changeOver = $('[data-kog-date="' + dateString + '"][data-kog-rate="' + rate + '"]').attr('data-kog-changeover');
                if (changeOver) {
                  css += ' day--changeover-' + changeOver;
                  switch(changeOver) {
                    case '0':
                      tooltip = Drupal.t('Available');
                      break;
                    case '1':
                      tooltip = Drupal.t('Only departure');
                      break;
                    case '2':
                      tooltip = Drupal.t('Only arrival');
                      break;
                    case '3':
                      tooltip = Drupal.t('No arrival or departure');
                      break;
                    default:
                      break;
                  }
                }
                if ($('[data-kog-date="' + dateString + '"][data-kog-rate="' + rate + '"]').attr('data-kog-available') > 0) {
                  css += ' day--available';
                }
                else {
                  css += ' day--unavailable';
                  tooltip = Drupal.t('Unavailable');
                }
                return [true, css, tooltip]
              }
            });
            $('.rate__toggler').click( function() {
              $(this).parent().find('.rate__detailheader').click();
            });
          },
          error: function () {
            $ajaxContainer.html(Drupal.t('Could not retrieve prices and availabilites. Please try reloading this page in a few minutes.'));
          }
        });
      });
      /**
       * Package rates 
       */
      $('[data-kog-rates="package"]', context).once('kognitiv_rate_package').each( function () {
        var $ajaxContainer = $(this);
        $.ajax({
          url: $ajaxContainer.attr('data-kog-rates-callback'),
          data: {'nid': $(this).attr('data-kog-rates-node')},
          dataType: 'json',
          async: true,
          cache: false,
          success: function (result) {
            $ajaxContainer.replaceWith(result.html);
            var calStart = $('[data-kog-calendar]').attr('data-kog-calendar-startdate');
            var calLength = $('[data-kog-calendar]').attr('data-kog-calendar-length');
            /**
             * Package rates detail calendar 
             */
            $('[data-kog-calendar="package"]').datepicker({
              minDate: new Date(calStart),
              maxDate: '+' + calLength + 'D',
              dateFormat: 'yy-mm-dd',
              onSelect: function(date) {
                if (!wtKognitiv.rate.arrival || (wtKognitiv.rate.arrival && wtKognitiv.rate.departure)) {
                  wtKognitiv.rate.arrival = date;
                  delete wtKognitiv.rate.departure;
                }
                else if (wtKognitiv.rate.arrival && date > wtKognitiv.rate.arrival) {
                  wtKognitiv.rate.departure = date;
                }
                else if (wtKognitiv.rate.arrival && date < wtKognitiv.rate.arrival) {
                  wtKognitiv.rate.departure = wtKognitiv.rate.arrival;
                  wtKognitiv.rate.arrival = date;
                }
                else if (wtKognitiv.rate.arrival == date) {
                  delete wtKognitiv.rate.arrival;
                  delete wtKognitiv.rate.departure;
                }
                var room = $(this).attr('data-kog-calendar-room');
                $('[data-kog-calendar]:not([data-kog-calendar-room="' + room + '"])').datepicker('refresh');
              },
              beforeShowDay: function (date) {
                var room = $(this).attr('data-kog-calendar-room');
                var dd = ('0' + date.getDate()).slice(-2);
                var mm = ('0' + (date.getMonth()+1)).slice(-2);
                var yyyy = date.getFullYear();
                var dateString = yyyy + '-' + mm + '-' + dd;
                var css = 'day';
                var tooltip = false;

                if (dateString == wtKognitiv.rate.arrival) {
                  css += ' day__selected day__selected--start';
                }
                else if (dateString == wtKognitiv.rate.departure) {
                  css += ' day__selected day__selected--end';
                }
                else if (dateString > wtKognitiv.rate.arrival && dateString < wtKognitiv.rate.departure) {
                  css += ' day__selected day__selected--middle';
                }

                var changeOver = $('[data-kog-date="' + dateString + '"][data-kog-room="' + room + '"]').attr('data-kog-changeover');
                if (changeOver) {
                  css += ' day--changeover-' + changeOver;
                  switch(changeOver) {
                    case '0':
                      tooltip = Drupal.t('Available');
                      break;
                    case '1':
                      tooltip = Drupal.t('Available, but no arrival');
                      break;
                    case '2':
                      tooltip = Drupal.t('Available, but no departure');
                      break;
                    case '3':
                      tooltip = Drupal.t('Available, but no arrival or departure');
                      break;
                    default:
                      break;
                  }
                }
                if ($('[data-kog-date="' + dateString + '"][data-kog-room="' + room + '"]').attr('data-kog-available') > 0 &&
                    $('[data-kog-date="' + dateString + '"][data-kog-room="' + room + '"]').attr('data-kog-dayprice') > 0
                ) {
                  css += ' day--available';
                }
                else {
                  css += ' day--unavailable';
                  tooltip = Drupal.t('Unavailable');
                }
                return [true, css, tooltip]
              }
            });
          },
          error: function () {
            $ajaxContainer.html(Drupal.t('Could not retrieve prices and availabilites. Please try reloading this page in a few minutes.'));
          }
        });
      });
    }
  };
}(jQuery));