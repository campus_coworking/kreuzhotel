<?php

namespace Drupal\wt_kognitiv\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationListInterface;

use Drupal\wt_kognitiv\Plugin\Field\FieldType\PackageRate;

/**
 * Plugin implementation of the 'kognitiv_package_rate' widget.
 *
 * @FieldWidget(
 *   id = "kognitiv_package_rate",
 *   label = @Translation("JSON endpoint and price mode"),
 *   field_types = {
 *     "kognitiv_package_rate"
 *   }
 * )
 */
class PackageRateWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $item = $items[$delta];

    $element['price_mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Show prices as'),
      '#default_value' => isset($items[$delta]->price_mode) ? $items[$delta]->price_mode : NULL,
      '#required' => FALSE,
      '#options' => [
        PackageRate::PRICE_MODE_PERSON => $this->t('price per person'),
        PackageRate::PRICE_MODE_DAY => $this->t('price in room\'s standard occupancy')
      ],
    ];

    $element['sort_rates'] = [
      '#type' => 'radios',
      '#title' => $this->t('Sort rates by'),
      '#default_value' => isset($items[$delta]->sort_rates) ? $items[$delta]->sort_rates : NULL,
      '#required' => FALSE,
      '#options' => [
        PackageRate::RATES_SORT_KOGNITIV => $this->t('position of rooms in Kognitiv'),
        PackageRate::RATES_SORT_DRUPAL => $this->t('weight of rooms in Drupal'),
        PackageRate::RATES_SORT_PRICE => $this->t('minimum price'),
        PackageRate::RATES_SORT_CODE => $this->t('room code'),
        PackageRate::RATES_SORT_NAME => $this->t('room name'),
      ],
    ];

    $element['uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('JSON API endpoint'),
      '#default_value' => isset($items[$delta]->uri) ? $items[$delta]->uri : NULL,
      '#required' => FALSE,
      '#maxlength' => 2048,
      '#description' => $this->t('Do not change this unless you have additional occupancy restrictions.')
    ];

    $element['#type'] = 'fieldset'; 

    return $element;
  }

}
