<?php

namespace Drupal\wt_kognitiv\Plugin\Field\FieldFormatter;

use Drupal\Core\Url;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

use Drupal\wt_kognitiv\Plugin\migrate_plus\data_fetcher\KognitivHttp;
use Drupal\wt_kognitiv\Controller\AjaxController;
use Drupal\wt_kognitiv\Plugin\Field\FieldType\PackageRate;

/**
 * Plugin implementation of the 'kognitiv_package_rate' formatter.
 *
 * @FieldFormatter(
 *   id = "kognitiv_package_rate",
 *   label = @Translation("Show package rates from Kognitiv channel manager"),
 *   field_types = {
 *     "kognitiv_package_rate"
 *   }
 * )
 */
class PackageRateFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    
    foreach ($items as $delta => $item) {

      $parent = $items->getEntity();
      if ($parent->bundle() != PackageRate::PACKAGE_BUNDLE_NAME || !$parent->get('remote_id')->getString() || $parent->get('remote_datasource')->getString() != KognitivHttp::REMOTE_DATASOURCE) {
        // don't output anything, handle missing and invalid rates like an empty field value
        // useful to mix manually created host bundles or mix different sources with Kognitiv 
        continue;
      }

      $ajaxBaseurl = Url::fromRoute('wt_kognitiv.package_rate');

      $element[$delta] = [
        '#markup' => '<div data-kog-rates="package" data-kog-rates-node="' . $parent->id() . '" data-kog-rates-callback="' . $ajaxBaseurl->toString() . '"><div class="text-align-center">' . $this->t('Loading prices and availabilities...') . '</div></div>',
        '#attached' => ['library'  => ['wt_kognitiv/rates']]
      ];
    }
    return $element;
  }
}