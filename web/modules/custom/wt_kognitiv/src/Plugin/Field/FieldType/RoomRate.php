<?php

namespace Drupal\wt_kognitiv\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'kognitiv_room_rate' field type.
 *
 * @FieldType(
 *   id = "kognitiv_room_rate",
 *   label = @Translation("Kognitiv room rate"),
 *   description = @Translation("Stores an URL to retrieve price and availability data for a room"),
 *   default_widget = "kognitiv_room_rate",
 *   default_formatter = "kognitiv_room_rate",
 *   cardinality = 1,
 *   column_groups = {
 *     "uri" = {
 *       "label" = @Translation("JSON API endpoint"),
 *       "translatable" = TRUE
 *  *     },
 *     "price_mode" = {
 *       "label" = @Translation("Show prices as")
 *     },
 *     "sort_rates" = {
 *       "label" = @Translation("Sort rates by")
 *     }
 *   }
 * )
 */
class RoomRate extends FieldItemBase {

  /**
   * bundle machine name for room type nodes
   */
  const ROOM_BUNDLE_NAME = 'wt_room';

  /**
   * Show prices for this room as price/person
   */
  const PRICE_MODE_PERSON = 'PERSON';

  /**
   * Show prices for this room as price/day
   */
  const PRICE_MODE_DAY = 'DAY';

  /**
   * Sort rates by Kognitiv rate code
   */
  const RATES_SORT_CODE = 'CODE';

  /**
   * Sort rates by meal plan
   */
  const RATES_SORT_MEALPLAN = 'MEALPLAN';

  /**
   * Sort rates by rate name
   */
  const RATES_SORT_NAME = 'NAME';

  /**
   * Sort rates by min price in std occupancy
   */
  const RATES_SORT_PRICE = 'PRICE';
  
  /**
   * Sort rates as given by Kognitiv
   */
  const RATES_SORT_KOGNITIV = 'POSITION_K';
  
    /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['uri'] = DataDefinition::create('string')
      ->setLabel(t('JSON API endpoint'));

    $properties['price_mode'] = DataDefinition::create('string')
      ->setLabel(t('Show prices as'));

    $properties['sort_rates'] = DataDefinition::create('string')
      ->setLabel(t('Sort rates by'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'uri' => [
          'description' => 'The URI of the JSON API endpoint.',
          'type' => 'varchar',
          'length' => 2048,
        ],
        'price_mode' => [
          'description' => 'Show prices as price per day or price per person in std occupancy',
          'type' => 'varchar',
          'length' => 10,
        ],
        'sort_rates' => [
          'description' => 'Sort rates by',
          'type' => 'varchar',
          'length' => 10,
        ],
      ],
      'indexes' => [],
      'foreign keys' => [],
    ];
  }

  /**
   * This field's values are only modifiers for the remotly fetched data, 
   * therefore this field is never treated as empty.
   */
  public function isEmpty() {
    return FALSE;
  }
}
