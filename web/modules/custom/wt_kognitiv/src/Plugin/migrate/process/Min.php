<?php

namespace Drupal\wt_kognitiv\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Gets the minimun value of an array. Source should be an array of
 * is_numeric() values like [5, 3.7, -2, +0123.45e6]
 *  
 * Available configuration keys:
 * - source: Source property.
 * 
 * Example:
 *
 * @code
 * process:
 *   field_minvalue:
 *     plugin: min
 *     source: my_numbers
 * @endcode
 *
 * @see \Drupal\migrate\Plugin\MigrateProcessInterface
 *
 * @MigrateProcessPlugin(
 *   id = "min",
 *   handle_multiples = TRUE
 *  * )
 */
class Min extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $min_value = null;
    if (is_array($value) || $value instanceof \Traversable) {
      foreach ($value as $sub_value) {
        if (is_numeric($sub_value)  && (!isset($min_value) || (float) $sub_value < $min_value)) {
          $min_value = (float) $sub_value;
        }
      }
    }
    elseif (is_numeric($value)) {
      $min_value = (float) $value;
    }
    return $min_value;
  }
}
