<?php

namespace Drupal\wt_kognitiv\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns the min price of a rate in standard occupancy
 * from a Kognitiv rates average query
 *  
 * Available configuration keys:
 * - source: Source property.
 * 
 * Example:
 *
 * @code
 * process:
 *   field_minprice:
 *     plugin: rate_min_price
 *     source: prices
 * @endcode
 *
 * @see \Drupal\migrate\Plugin\MigrateProcessInterface
 *
 * @MigrateProcessPlugin(
 *   id = "rate_min_price",
 *   handle_multiples = TRUE
 *  * )
 */
class RateMinPrice extends ProcessPluginBase implements ContainerFactoryPluginInterface {
 
  /**
   * @var EntityTypeManager
   */
  protected $entityTypeManager; 

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManager $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $rate_min_price = null;
    $std_occupancy = is_int($this->configuration['occupancy']) ? $this->configuration['occupancy'] : 1;
    if (is_array($value)) {
      foreach ($value as $rate) {
        // to calculate price/person in std occupancy
        if (!is_int($this->configuration['occupancy']) && array_key_exists('code', $rate)) {
          $nodes = $this->entityTypeManager
            ->getStorage('node')
            ->loadByProperties([
              'type' => 'room',
              'remote_datasource' => 'kognitiv',
              'remote_id' => $rate['code']
            ]);
          if ($node = reset($nodes)) {
            if ($node->hasField('room_std_occupancy')) {
              $std_occupancy = $node->get('room_std_occupancy')->value ?: 1;
            }
          }
        }
        if (array_key_exists('prices', $rate)) {
          foreach ($rate['prices'] as $month) {
            if (is_array($month) && array_key_exists(0, $month) && is_numeric($month[0])) {
              $current_price = (float) $month[0] / $std_occupancy;
              if (!isset($rate_min_price) || $current_price < $rate_min_price) {
                $rate_min_price = $current_price;
              }
            }
          }
        }
      }
    }
    return $rate_min_price;
  }
}
