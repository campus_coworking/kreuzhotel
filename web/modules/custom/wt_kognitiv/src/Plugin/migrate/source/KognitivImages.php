<?php

namespace Drupal\wt_kognitiv\Plugin\migrate\source;

use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Plugin\migrate\source\SourcePluginBase;
use Drupal\migrate_plus\Plugin\migrate\source;

/**
 * Source plugin for retrieving Kognitiv Images.
 *
 * Example:
 *
 * @code
 * source:
 *   plugin: kognitiv_images
 *   json_url: https://www.example.com/@PROPERTY_CODE@/endpoint.json
 *   images_baseurl: https://www.example.com/prepend/@PROPERTY_CODE@/
 * @endcode
 * 
 * @MigrateSource(
 *   id = "kognitiv_images"
 * )
 */
class KognitivImages extends \Drupal\migrate_plus\Plugin\migrate\source\Url {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    $configuration['urls'] = [];
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
    $this->jsonUrl = $configuration['json_url'];
    $this->imagesBaseUrl = $configuration['image_baseurl'];
  }


  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    $dataFetcher = \Drupal::service('plugin.manager.migrate_plus.data_fetcher')->createInstance('kognitiv_http', $this->configuration);
    $response = $dataFetcher->getResponseContent($this->jsonUrl);
    $json = json_decode($response);
    $flatResult = [];
    foreach ($json->result->pictures as $key => $value) {
      $flatResult[] = ['filename' => $key, 'source_uri' => $dataFetcher->replaceUrlTokens($this->imagesBaseUrl) . $key]; 
    }
    $flatObj = new \ArrayObject($flatResult);
    $this->imagesIterator = $flatObj->getIterator(); 
    return $this->imagesIterator;
  }
}
