<?php

namespace Drupal\wt_kognitiv\Plugin\migrate_plus\data_fetcher;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateException;
use Drupal\migrate_plus\DataFetcherPluginBase;
use GuzzleHttp\Exception\RequestException;
use Drupal\migrate_plus\Plugin\migrate_plus\data_fetcher;

/**
 * Retrieve data from Kognitiv JSON API over an HTTP connection with token authentication.
 *
 *  * Example:
 *
 * @code
 * source:
 *   plugin: url
 *   data_fetcher_plugin: kognitiv_http
 *   headers:
 *     Accept: application/json
 * @endcode
 *
 * @DataFetcher(
 *   id = "kognitiv_http",
 *   title = @Translation("Kognitiv JSON API based on Migrate plus HTTP")
 * )
 */
class KognitivHttp extends \Drupal\migrate_plus\Plugin\migrate_plus\data_fetcher\Http {

  /**
   * All room nodes importet from Kognitiv have a text field "remote_datasource" with this value
   */
  const REMOTE_DATASOURCE = 'KOGNITIV';

  protected $config;
  protected $cacheBackend;


  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    
    $this->config = \Drupal::config('wt_kognitiv.settings');
    $this->cacheBackend = \Drupal::cache('wt_kognitiv');
  }

  /**
   * Returns an access token for the Kognitiv JSON API, which is required for all successive queries
   */
  public function getAccessToken() {
    if ($cachedToken = $this->cacheBackend->get('wt_kognitiv:token')) {
      return $cachedToken->data;
    }
    
    $tokenUrl = strtr($this->config->get('token_url'), [
      '@PROPERTY_CODE@' => $this->config->get('property_code'),
      '@API_KEY@' => $this->config->get('api_key'),
    ]);
    $jsonString = (string) parent::getResponse($tokenUrl)->getBody();
    $json = json_decode($jsonString);
    if ($json->success && $token = $json->access_token) {
      $tokenLifetime = $this->config->get('token_lifetime');
      $this->cacheBackend->set('wt_kognitiv:token', $token, time() + $tokenLifetime);
      return $token;
    }

    return false;
  }
  
  protected function getTokenMap() {
    return [
      '@PROPERTY_CODE@' => $this->config->get('property_code'),
      '@API_KEY@' => $this->config->get('api_key'),
      '@TOKEN@' => $this->getAccessToken(),
      '@TODAY@' => strftime('%Y-%m-%d'),
      '@OVERVIEW_LENGTH@' => $this->config->get('overview_length'),
      '@OVERVIEW_MONTH@' => (int) ($this->config->get('overview_length') / 30),
      '@LANG@' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
    ];
  }

  /**
   * Replaces placeholder tokens in Kognitiv API Urls with configuration values
   */
  public function replaceUrlTokens($url) {
    return strtr($url, $this->getTokenMap());
  }

  /**
   * Generates a cache key by url tokens
   */
  protected function getCacheKeyByQuery($url) {
    $result = '';
    parse_str(parse_url($url, PHP_URL_QUERY), $params);
    ksort($params);
    foreach ($params as $key => $value) {
      if (strpos($url, $key) && strtolower($key) != 'token') {
        $result .= ':' . $value;
      }
    }
    return $result;
  }


  /**
   * {@inheritdoc}
   */
  public function getResponse($url) {
    return parent::getResponse($this->replaceUrlTokens($url));
  }

  /**
   * Gets the response body from a Kognitiv API call.
   * By default all responses will be cached for a token lifetime.
   */
  public function getResponseContent($url, $cacheTime = NULL) {
    if (is_null($cacheTime)) {
      $cacheTime = $this->config->get('token_lifetime');
    }
    $parsedUrl = $this->replaceUrlTokens($url);
    $cacheKey = 'wt_kognitiv:' . parse_url($parsedUrl, PHP_URL_PATH) . $this->getCacheKeyByQuery($parsedUrl);
    if ($cacheTime && $cachedJson = $this->cacheBackend->get($cacheKey)) {
      $jsonString = $cachedJson->data;
    }
    else {
      $jsonString = (string) parent::getResponse($parsedUrl)->getBody();
      $this->cacheBackend->set($cacheKey, $jsonString, time() + $cacheTime);

      if (strpos($url, 'ratesAverage') > 0 && strpos($jsonString, '{"success":true,"result":{') === 0) {
        $json = json_decode(html_entity_decode(html_entity_decode($jsonString)), true);
        $packages = [];
        foreach ($json['result']['packages'] as $package) {
          $packages[] = $package['code'];
        }
        $this->cacheBackend->set('wt_kognitiv:active_packages', implode(',', $packages), time() + $cacheTime);
      }
    }
    return $jsonString;
  }
}
