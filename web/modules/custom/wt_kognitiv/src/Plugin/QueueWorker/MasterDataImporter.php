<?php
namespace Drupal\wt_kognitiv\Plugin\QueueWorker;

/**
 * @QueueWorker(
 *   id = "wt_kognitiv_master_data_cron",
 *   title = @Translation("Kognitiv Importer - Master Data"),
 *   cron = {"time" = 60}
 * )
 *
 */
class MasterDataImporter extends DataImporterBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $this->getRoomsAndPackages($data);
  }
}