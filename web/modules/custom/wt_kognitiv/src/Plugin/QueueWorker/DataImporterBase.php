<?php

namespace Drupal\wt_kognitiv\Plugin\QueueWorker;

use Drupal\Core\State\StateInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use \GuzzleHttp\Client;

/**
 * Provides core functionality for the Kognitiv Importer.
 */
abstract class DataImporterBase extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * URL to retrieve access tokens for Kognitiv JSON API
   */
  private $tokenUrl = 'https://cloud.seekda.com/w/w-dynamic-shop/hotel:@PROPERTY_CODE/@API_KEY.json';
  
  /**
   * Lifetime of an access token (60 minutes according to https://connectivity.seekda.com/json/token_service)
   */
  const KOGNITIV_TOKEN_LIFETIME = 3600;

  /**
   * URL to offersOverview.json API, for detailled prices and availabilities of all rates
   */
  private $offersOverviewUrl = 'https://switch.seekda.com/switch/latest/json/offersOverview.json?skd-property-code=@PROPERTY_CODE&skd-start-date=@START_DATE&skd-overview-length=@NUMBER_OF_DAYS&skd-overview-type=rate&token=@ACCESS_TOKEN';
  
  /**
   * URL to ratesAverage.json API, for an overview of available rooms and packages, and their assigned rates
   */
  private $ratesAverageUrl = 'https://switch.seekda.com/switch/latest/json/ratesAverage.json?skd-property-code=@PROPERTY_CODE&skd-start=@START_DATE&skd-months=@NUMBER_OF_MONTHS&skd-packages=true&skd&token=@ACCESS_TOKEN';
  
  /**
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;


  /**
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;


  /**
   * DataImporterBase constructor.
   *
   * @param array $configuration
   *   The configuration of the instance.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service the instance should use.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger service the instance should use.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, StateInterface $state, LoggerChannelFactoryInterface $loggerFactory, CacheBackendInterface $cacheBackend) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->state = $state;
    $this->logger = $loggerFactory->get('wt_kognitiv');
    $this->cacheBackend = $cacheBackend;
    $this->config = \Drupal::config('wt_kognitiv.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('state'),
      $container->get('logger.factory'),
      $container->get('cache.wt_kognitiv')
    );
  }

  /**
   * Returns an access token for the Kognitiv JSON API, which is required for all successive queries
   */
  private function getAccessToken() {
    try {
      if ($cache = $this->cacheBackend->get('wt_kognitiv:token')) {
        return $cache->data;
      }
      else {
        $this->tokenUrl = strtr($this->tokenUrl, [
          '@PROPERTY_CODE' => $this->config->get('property_code'),
          '@API_KEY' => $this->config->get('api_key')
        ]
        );
        $guzzle = new Client();
        $response = $guzzle->get($this->tokenUrl);
        $json = json_decode($response->getBody());
        if ($json && $json->success == 'true') {
          $this->cacheBackend->set('wt_kognitiv:token', $json->access_token, time() + DataImporterBase::KOGNITIV_TOKEN_LIFETIME);
          return $json->access_token;
        }
        else {
          throw new \Exception('Unexpected response from JSON-API ' . $this->tokenUrl);
        }
      }
    }
    catch (\Exception $e) {
      $this->logger->warning('Could not retrieve access token from Kognitiv. \n' . $e->getMessage());
      drupal_set_message(t('Could not retrieve access token from Kognitiv.'), 'warning');
    }
  }


  /**
   * Returns an array of rates with detailed prices and availabilities 
   * using Kognitiv's offersOverview.json API
   */
  public function getRatesDetails($data) {
    try {
      $cacheId = 'wt_kognitiv:offersoverview_json';
      if ($data && $data['cacheContext']) {
        $cacheId = 'wt_kognitiv:offersoverview_json:' . $data['cacheContext'];
      }
      
      if ($cache = $this->cacheBackend->get($cacheId)) {
        return $cache->data;
      }
      elseif ($this->getAccessToken()) {
        $this->offersOverviewUrl = strtr($this->offersOverviewUrl, [
          '@PROPERTY_CODE' => $this->config->get('property_code'),
          '@START_DATE' => date('Y-m-d'),
          '@NUMBER_OF_DAYS' => $this->config->get('number_of_days'),
          '@ACCESS_TOKEN' => $this->getAccessToken(),
        ]
        );
        if ($data && $data['urlParameters']) {
          foreach ($data['urlParameters'] as $key => $value) {
            $this->offersOverviewUrl .= '&' . urlencode($key) . '=' . urlencode($value);
          }
        }
        $guzzle = new Client();
        $response = $guzzle->get($this->offersOverviewUrl);
        $json = json_decode($response->getBody());
        if ($json && $json->overview && $json->overview->startDate == date('Y-m-d')) {
          $this->cacheBackend->set($cacheId, $json->overview->entities, time() + $this->config->get('ratesdata_lifetime'));
          return $json->overview->entities;
        }
        else {
          throw new \Exception('Unexpected response from JSON-API ' . $this->offersOverviewUrl);
        }
      }
    }
    catch (\Exception $e) {
      $this->logger->warning('Could not retrieve rates details from Kognitiv. \n' . $e->getMessage());
      drupal_set_message(t('Could not retrieve rates details from Kognitiv.'), 'warning');
    }
  }


  /**
   * Returns an array of rates with detailed prices and availabilities 
   * using Kognitiv's offersOverview.json API
   */
  public function getRoomsAndPackages($data) {
    try {
      $cacheId = 'wt_kognitiv:ratesaverage_json';
      if ($data && $data['cacheContext']) {
        $cacheId = 'wt_kognitiv:ratesaverage_json:' . $data['cacheContext'];
      }
  
      if ($cache = $this->cacheBackend->get($cacheId)) {
        return $cache->data;
      }
      elseif ($this->getAccessToken()) {
        $this->ratesAverageUrl = strtr($this->ratesAverageUrl, [
          '@PROPERTY_CODE' => $this->config->get('property_code'),
          '@START_DATE' => date('Y-m-d'),
          '@NUMBER_OF_MONTHS' => $this->config->get('number_of_days')/30,
          '@ACCESS_TOKEN' => $this->getAccessToken(),
        ]
        );
        if ($data && $data['urlParameters']) {
          foreach ($data['urlParameters'] as $key => $value) {
            $this->offersOverviewUrl .= '&' . urlencode($key) . '=' . urlencode($value);
          }
        }
        $guzzle = new Client();
        $response = $guzzle->get($this->ratesAverageUrl);
        $json = json_decode($response->getBody());
        if ($json && $json->success == 'true') {
          $this->cacheBackend->set($cacheId, $json->result, time() + $this->config->get('masterdata_lifetime'));
          return $json->result;
        }
        else {
          throw new \Exception('Unexpected response from JSON-API ' . $this->ratesAverageUrl);
        }
      }
    }
    catch (\Exception $e) {
      $this->logger->warning('Could not retrieve rooms and packages from Kognitiv. \n' . $e->getMessage());
      drupal_set_message(t('Could not retrieve rooms and packages from Kognitiv.'), 'warning');
    }
  }
}