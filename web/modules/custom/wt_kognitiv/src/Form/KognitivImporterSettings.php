<?php

namespace Drupal\wt_kognitiv\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\wt_kognitiv\Plugin\Field\FieldType\RoomRate;
use Drupal\wt_kognitiv\Plugin\Field\FieldType\PackageRate;

class KognitivImporterSettings extends ConfigFormBase {

  /**
   * @var \Drupal\Core\CacheCacheBackendInterface
   */
  protected $cacheBackend;
  

  public function __construct(CacheBackendInterface $cacheBackend) {
    $this->cacheBackend = $cacheBackend;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cache.wt_kognitiv')
    );
  }
  
  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'wt_kognitiv.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wt_kognitiv_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('wt_kognitiv.settings');

    $form['property_code'] = array(
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#required' => true,
      '#title' => $this->t('Property code'),
      '#default_value' => $config->get('property_code'),
      '#description' => $this->t('Can be found in the <a href="https://cm.seekda.com" target="_blank">Kognitiv channel manager</a>, on the "Dynamic Shop > Integration" page, in the script tag section.<br>In the following example the property code is highlighted:<br>&lt;script src="https://cloud.seekda.com/w/w-dynamic-shop/hotel:<b style="color: red;">YOUR_PROPERTY_CODE</b>/your-api-key-0123456abcdef.js"&gt;</script>'),
    );

    $form['api_key'] = array(
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#required' => true,
      '#title' => $this->t('API key'),
      '#default_value' => $config->get('api_key'),
      '#description' => $this->t('Can be found in the <a href="https://cm.seekda.com" target="_blank">Kognitiv channel manager</a>, on the "Dynamic Shop > Integration" page, in the script tag section.<br>In the following example the API key is highlighted:<br>&lt;script src="https://cloud.seekda.com/w/w-dynamic-shop/hotel:YOUR_PROPERTY_CODE/<b style="color: red;">your-api-key-0123456abcdef</b>.js"&gt;</script>'),
    );

    $form['overview_length'] = array(
      '#type' => 'number',
      '#min' => 30,
      '#max' => 360,
      '#step' => 30,
      '#required' => true,
      '#title' => $this->t('Rates preview days'),
      '#default_value' => $config->get('overview_length'),
      '#description' => $this->t('Number of days to show availabily and price data ahead from today.'),
    );

    $form['masterdata_lifetime'] = array(
      '#type' => 'number',
      '#min' => 60*60,
      '#max' => 60*60*24*360,
      '#step' => 1,
      '#required' => true,
      '#title' => $this->t('Master data lifetime'),
      '#default_value' => $config->get('masterdata_lifetime'),
      '#description' => $this->t('Time in seconds before Drupal\'s cron job updates master data (such as rooms, images,...).'),
    );

    $form['ratesdata_lifetime'] = array(
      '#type' => 'number',
      '#min' => 0,
      '#max' => 60*60*24,
      '#step' => 1,
      '#required' => true,
      '#title' => $this->t('Rates data lifetime'),
      '#default_value' => $config->get('ratesdata_lifetime'),
      '#description' => $this->t('Time in seconds to cache rates data (prices and availability).'),
    );  

    $form['price_mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('By default show prices for rates as'),
      '#required' => true,
      '#default_value' => $config->get('price_mode'),
      '#options' => [
        RoomRate::PRICE_MODE_PERSON => $this->t('price per person'),
        RoomRate::PRICE_MODE_DAY => $this->t('price per day')
      ],
      '#description' => $this->t('Price caluclations are always based on the room\'s standard occupancy for adults.'),
    ];

    $form['sort_rates_room'] = [
      '#type' => 'radios',
      '#title' => $this->t('By default sort room rates by'),
      '#required' => true,
      '#default_value' => $config->get('sort_rates_room'),
      '#options' => [
        RoomRate::RATES_SORT_KOGNITIV => $this->t('position in Kognitiv'),
        RoomRate::RATES_SORT_PRICE => $this->t('minimum price'),
        RoomRate::RATES_SORT_CODE => $this->t('rate code'),
        RoomRate::RATES_SORT_NAME => $this->t('rate name'),
        RoomRate::RATES_SORT_MEALPLAN => $this->t('meal plan'),
      ],
    ];

    $form['sort_rates_package'] = [
      '#type' => 'radios',
      '#title' => $this->t('By default sort package rates by'),
      '#required' => true,
      '#default_value' => $config->get('sort_rates_package'),
      '#options' => [
        PackageRate::RATES_SORT_KOGNITIV => $this->t('position of rooms in Kognitiv'),
        PackageRate::RATES_SORT_DRUPAL => $this->t('weight of rooms in Drupal'),
        PackageRate::RATES_SORT_PRICE => $this->t('minimum price'),
        PackageRate::RATES_SORT_CODE => $this->t('room code'),
        PackageRate::RATES_SORT_NAME => $this->t('room name'),
      ],
    ];

    $form['reset_cache'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Reset data lifetime'),
      '#description' => $this->t('Forces a fresh data fetch from Kognitiv for all data.'),
    );  

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Config\Config $config */
    $config = \Drupal::service('config.factory')->getEditable('wt_kognitiv.settings');
    $config->set('property_code', $form_state->getValue('property_code'));
    $config->set('api_key', $form_state->getValue('api_key'));
    $config->set('masterdata_lifetime', $form_state->getValue('masterdata_lifetime'));
    $config->set('ratesdata_lifetime', $form_state->getValue('ratesdata_lifetime'));
    $config->set('overview_length', $form_state->getValue('overview_length'));
    $config->set('price_mode', $form_state->getValue('price_mode'));
    $config->set('sort_rates_room', $form_state->getValue('sort_rates_room'));
    $config->set('sort_rates_package', $form_state->getValue('sort_rates_package'));
    
    if ($form_state->getValue('reset_cache')) {
      $this->cacheBackend->invalidateAll();
      \Drupal::state()->set('wt_kognitiv.next_masterdata_import', 0);
      drupal_set_message($this->t('Master data will be updated with Drupal\'s next cron run and rates data cache has been cleared.'));
    }

    parent::submitForm($form, $form_state);

    $config->save();
    drupal_set_message($this->t('The configuration options have been saved.'));
  }

}
