<?php

namespace Drupal\wt_kognitiv\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

use Drupal\wt_kognitiv\Plugin\Field\FieldType\RoomRate;
use Drupal\wt_kognitiv\Plugin\Field\FieldType\PackageRate;
use Drupal\wt_kognitiv\Plugin\migrate_plus\data_fetcher\KognitivHttp;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * An example controller.
 */
class AjaxController extends ControllerBase implements ContainerInjectionInterface {

  var $kognitivPackages = NULL;

  /**
   * {@inheritdoc}
   */
  public function __construct(RendererInterface $renderer) {
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer')
    );
  }

  private function getKognitivPackages() {
    if (isset($this->kognitivPackages)) {
      return $this->kognitivPackages;
    }
    
    $nodeList = $this->entityTypeManager()
      ->getStorage('node')
      ->loadByProperties([
        'type' => 'wt_package',
        'remote_datasource' => KognitivHttp::REMOTE_DATASOURCE
      ]);
    foreach ($nodeList as $node) {
      if ($packageCode = $node->remote_id->value) {
        $this->kognitivPackages[] = $packageCode;
      }
    }
    return $this->kognitivPackages;
  }
  
  /**
   * {@inheritdoc}
   */
  public function getRoomRates(Request $request) {
    $ratesLifetime = $this->config('wt_kognitiv.settings')->get('ratesdata_lifetime');
    $nid = (int) $request->query->get('nid');
    $node = $this->entityTypeManager()->getStorage('node')->load($nid);

    try {
      if ($node->bundle() != RoomRate::ROOM_BUNDLE_NAME || !$node->get('remote_id')->getString() || $node->get('remote_datasource')->getString() != KognitivHttp::REMOTE_DATASOURCE) {
        throw new \Exception("ERROR: This page is either no room or is missing fields to retrieve remote data, can't show rates.");
      }
      if (!$node->hasField('field_room_rate')) {
        throw new \Exception("ERROR: Rates field is missing or not configured.");
      }
    }
    catch (\Exception $e) {
      $response = new JsonResponse(['success' => FALSE, 'message' => $e->getMessage()], 500);
      $response->setMaxAge(0);
      return $response;
    }
    
    $ratesUrl = $node->get('field_room_rate')->uri ?: $this->config('wt_kognitiv.settings')->get('rates_url');
    $priceMode = $node->get('field_room_rate')->price_mode ?: $this->config('wt_kognitiv.settings')->get('price_mode');
    $sortRates = $node->get('field_room_rate')->sort_rates ?: $this->config('wt_kognitiv.settings')->get('sort_rates_room');

    $dataFetcher = \Drupal::service('plugin.manager.migrate_plus.data_fetcher')->createInstance('kognitiv_http', []);
    $responseContent = $dataFetcher->getResponseContent($ratesUrl, $ratesLifetime);
    $json = json_decode($responseContent, TRUE);
    $entities = $json['overview']['entities'];
    
    // we want a single room's dayprice rates without packages or promotions
    $roomCode = $node->get('remote_id')->getString();
    $packages = $this->getKognitivPackages();
    $filteredRoomRates = array_filter($entities, function($rate) use ($roomCode, $packages) {
      $validPrices = array_filter($rate['prices']);
      return ($rate['room_code'] == $roomCode && 
        !in_array($rate['rate_code'], $packages) &&
        !preg_match('/PR.+\$.+/', $rate['rate_code']) &&
        count($validPrices)
      );
    });
    $roomRates = [];
    foreach ($filteredRoomRates as $rate) {
      $roomRates[$rate['rate_code']] = $rate;
      $roomRates[$rate['rate_code']]['min_price'] = min(array_filter($rate['prices']));
    }

    $responseContent = $dataFetcher->getResponseContent(
      $this->config('wt_kognitiv.settings')->get('ratenames_url'), 
      $this->config('wt_kognitiv.settings')->get('ratesdata_lifetime')
    );
    $masterdataJson = json_decode($responseContent, TRUE);
    $rooms = $masterdataJson['result']['rooms'];
    foreach ($rooms as $room) {
      if ($room['code'] != $roomCode) {
        continue;
      }
      foreach ($room['rates'] as $rate) {
        $roomRates[$rate['code']]['rate_name'] = html_entity_decode($rate['title']);
        $roomRates[$rate['code']]['rate_description'] = html_entity_decode($rate['description']);
      }
    }

    switch ($sortRates) {
      case RoomRate::RATES_SORT_PRICE:
        uasort($roomRates, function($a, $b) {
          return ($a['min_price'] < $b['min_price']) ? -1 : 1;
        });
        break;
      case RoomRate::RATES_SORT_CODE:
        uasort($roomRates, function($a, $b) {
          return ($a['code'] < $b['code']) ? -1 : 1;
        });
        break;
      case RoomRate::RATES_SORT_NAME:
        uasort($roomRates, function($a, $b) {
          return ($a['rate_name'] < $b['rate_name']) ? -1 : 1;
        });
        break;
      case RoomRate::RATES_SORT_MEALPLAN:
        uasort($roomRates, function($a, $b) {
          return ($a['meal_plan_code'] < $b['meal_plan_code']) ? -1 : 1;
        });
        break;
      case RoomRate::RATES_SORT_KOGNITIV:
      default:
        break;
    }
    
    $renderable = [
      'json' => [
        '#theme' => 'room__rate',
        '#node_id' => $nid,
        '#room_code' => $roomCode,
        '#query_startdate' => $json['overview']['startDate'],
        '#query_length' => $this->config('wt_kognitiv.settings')->get('overview_length'),
        '#price_mode' => $node->get('field_room_rate')->price_mode,
        '#rates' =>$roomRates,
      ],
      '#cache'  => ['max-age' => $ratesLifetime]
    ];
    $response = new JsonResponse(['html' => $this->renderer->render($renderable)], 200);
    $response->setMaxAge($ratesLifetime);
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function getPackageRates(Request $request) {
    $ratesLifetime = $this->config('wt_kognitiv.settings')->get('ratesdata_lifetime');
    $nid = (int) $request->query->get('nid');
    $node = $this->entityTypeManager()->getStorage('node')->load($nid);

    try {
      if ($node->bundle() != PackageRate::PACKAGE_BUNDLE_NAME || !$node->get('remote_id')->getString() || $node->get('remote_datasource')->getString() != KognitivHttp::REMOTE_DATASOURCE) {
        throw new \Exception("ERROR: This page is either no package or is missing fields to retrieve remote data, can't show rates.");
      }
      if (!$node->hasField('field_package_rate')) {
        throw new \Exception("ERROR: Rates field is missing or not configured.");
      }
    }
    catch (\Exception $e) {
      $response = new JsonResponse(['success' => FALSE, 'message' => $e->getMessage()], 500);
      $response->setMaxAge(0);
      return $response;
    }
    
    $ratesUrl = $node->get('field_package_rate')->uri ?: $this->config('wt_kognitiv.settings')->get('rates_url');
    $priceMode = $node->get('field_package_rate')->price_mode ?: $this->config('wt_kognitiv.settings')->get('price_mode');
    $sortRates = $node->get('field_package_rate')->sort_rates ?: $this->config('wt_kognitiv.settings')->get('sort_rates_package');

    $dataFetcher = \Drupal::service('plugin.manager.migrate_plus.data_fetcher')->createInstance('kognitiv_http', []);
    $responseContent = $dataFetcher->getResponseContent($ratesUrl, $ratesLifetime);
    $json = json_decode($responseContent, TRUE);
    $entities = $json['overview']['entities'];
    
    // we want a single room's dayprice rates without packages or promotions
    $packageCode = $node->get('remote_id')->getString();
    $filteredPackageRates = array_filter($entities, function($rate) use ($packageCode) {
      $validPrices = array_filter($rate['prices']);
      return ($rate['rate_code'] == $packageCode && count($validPrices));
    });
    $packageRates = [];
    foreach ($filteredPackageRates as $rate) {
      $packageRates[$rate['room_code']] = $rate;
      $packageRates[$rate['room_code']]['min_price'] = min(array_filter($rate['prices']));
      $packageRates[$rate['room_code']]['room_node'] = false;
      
      if ($node->hasField('field_room')) {
        foreach ($node->get('field_room') as $er) {
          $erNode = $er->get('entity')->getTarget()->getValue();
          $lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
          if ($erNode->hasTranslation($lang)) {
            $erNode = $erNode->getTranslation($lang);
          }
          if ($erNode->bundle() == RoomRate::ROOM_BUNDLE_NAME && $erNode->hasField('remote_id') && $erNode->get('remote_id')->value == $rate['room_code']) {
            $packageRates[$rate['room_code']]['room_node'] = $erNode->toArray();
            $packageRates[$rate['room_code']]['room_image'] = false;
            if (!$erNode->get('field_image')->isEmpty()) {
              $mediaEntity = $erNode->get('field_image')->first()->get('entity')->getTarget()->getValue();
              $image = $mediaEntity->get('field_media_image')->first()->get('entity')->getTarget()->getValue();
              $packageRates[$rate['room_code']]['room_image'] = $image->toArray();
            }
          }
        }
      }
    }

    switch ($sortRates) {
      case PackageRate::RATES_SORT_PRICE:
        uasort($packageRates, function($a, $b) {
          return ($a['min_price'] < $b['min_price']) ? -1 : 1;
        });
        break;
      case PackageRate::RATES_SORT_CODE:
        uasort($packageRates, function($a, $b) {
          return ($a['room_code'] < $b['room_code']) ? -1 : 1;
        });
        break;
      case PackageRate::RATES_SORT_NAME:
        uasort($packageRates, function($a, $b) {
          return ($a['name'] < $b['name']) ? -1 : 1;
        });
        break;
      case PackageRate::RATES_SORT_DRUPAL:
      case PackageRate::RATES_SORT_KOGNITIV:
      default:
        break;
    }
    
    $renderable = [
      'json' => [
        '#theme' => 'package__rate',
        '#node_id' => $nid,
        '#package_code' => $packageCode,
        '#query_startdate' => $json['overview']['startDate'],
        '#query_length' => $this->config('wt_kognitiv.settings')->get('overview_length'),
        '#price_mode' => $node->get('field_package_rate')->price_mode,
        '#rates' => $packageRates,
      ],
      '#cache'  => ['max-age' => $ratesLifetime]
    ];
    $response = new JsonResponse(['html' => $this->renderer->render($renderable)], 200);
    $response->setMaxAge($ratesLifetime);
    return $response;
  }
}