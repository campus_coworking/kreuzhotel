<?php

namespace Drupal\wt_season\CacheContext;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
* Class SeasonCacheContext.
*/
class SeasonCacheContext implements CacheContextInterface {


  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Entity containing project-specific settings
   *
   * @var \Drupal\config_pages\Entity\ConfigPages
   */
  protected $projectSettings;

  /**
   * Constructs a new SeasonCacheContext object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $list = $this->entityTypeManager->getStorage('config_pages')->loadByProperties(['type' => 'project']);
    $this->projectSettings = $list ? current($list) : NULL;
  }

  /**
  * {@inheritdoc}
  */
  public static function getLabel() {
    drupal_set_message('Hide or show paragraphs based on the current season');
  }

  /**
  * {@inheritdoc}
  */
  public function getContext() {
    $season = 'any';
    if ($this->projectSettings && $this->projectSettings->hasField('field_summerstart') && $this->projectSettings->hasField('field_summerend')) {
      $summerStart = new \DateTime($this->projectSettings->get('field_summerstart')->value);
      $summerEnd = new \DateTime($this->projectSettings->get('field_summerend')->value);
      $now = new \Datetime();
      if ($summerStart->format('md') <= $now->format('md') && $summerEnd->format('md') >= $now->format('md')) {
        $season = 'summer';
      }
      else {
        $season = 'winter';
      }
    }
    return $season;
  }
  
  /**
  * {@inheritdoc}
  */
  public function getCacheableMetadata() {
    $cacheable_metadata = new CacheableMetadata();
    if ($this->projectSettings) {
      $cacheable_metadata->setCacheTags(['config_pages:' . $this->projectSettings->id()]);
    }
    return $cacheable_metadata;
  }
}
