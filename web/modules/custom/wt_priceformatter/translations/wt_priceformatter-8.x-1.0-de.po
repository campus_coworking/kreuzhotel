# $Id$
#
# LANGUAGE translation of Drupal (general)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  modules/custom/wt_priceformatter/wt_priceformatter.info.yml: n/a
#  modules/custom/wt_priceformatter/wt_priceformatter.links.menu.yml: n/a
#  modules/custom/wt_priceformatter/wt_priceformatter.routing.yml: n/a
#  modules/custom/wt_priceformatter/config/schema/wt_priceformatter.schema.yml: n/a
#  modules/custom/wt_priceformatter/src/Form/PriceFormatterSettings.php: n/a
#  modules/custom/wt_priceformatter/config/install/wt_priceformatter.settings.yml: n/a
#
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2018-03-01 16:49+0100\n"
"PO-Revision-Date: 2018-03-01 17:54+0100\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.0.5\n"
"Last-Translator: \n"
"Language: de\n"

#: modules/custom/wt_priceformatter/wt_priceformatter.info.yml:0
msgid "Webtourismus Price Formatter"
msgstr "Webtourismus Preis-Formatierer"

#: modules/custom/wt_priceformatter/wt_priceformatter.info.yml:0
msgid "Twig filter to print a number with heavy HTML markup for CSS styling"
msgstr ""
"Twig Filter um eine Zahl mit umfangreichen HMTL Markup auszugeben, für CSS "
"Anpassungen"

#: modules/custom/wt_priceformatter/wt_priceformatter.info.yml:0
msgid "Webtourismus.at"
msgstr "Webtourismus.at"

#: modules/custom/wt_priceformatter/wt_priceformatter.links.menu.yml:0
#: modules/custom/wt_priceformatter/wt_priceformatter.routing.yml:0
msgid "Price formatter"
msgstr "Preis-Formatierer"

#: modules/custom/wt_priceformatter/wt_priceformatter.links.menu.yml:0
msgid "Configure rendering of prices"
msgstr "Preisausgabe konfigurieren"

#: modules/custom/wt_priceformatter/config/schema/wt_priceformatter.schema.yml:0
msgid "Price formatter settings"
msgstr "Preis-Formatierer Einstellungen"

#: modules/custom/wt_priceformatter/config/schema/wt_priceformatter.schema.yml:0
#: modules/custom/wt_priceformatter/src/Form/PriceFormatterSettings.php:41
msgid "Number of decimal places"
msgstr "Anzahl Nachkommastellen"

#: modules/custom/wt_priceformatter/config/schema/wt_priceformatter.schema.yml:0
#: modules/custom/wt_priceformatter/src/Form/PriceFormatterSettings.php:51
msgid "Additional integer rounding"
msgstr "Zusätzliches Runden auf ganze Zahlen"

#: modules/custom/wt_priceformatter/config/schema/wt_priceformatter.schema.yml:0
#: modules/custom/wt_priceformatter/src/Form/PriceFormatterSettings.php:59
msgid "Decimal symbol"
msgstr "Dezimal-Symbol"

#: modules/custom/wt_priceformatter/config/schema/wt_priceformatter.schema.yml:0
#: modules/custom/wt_priceformatter/src/Form/PriceFormatterSettings.php:66
msgid "Text in front of the price"
msgstr "Text vor dem Preis"

#: modules/custom/wt_priceformatter/config/schema/wt_priceformatter.schema.yml:0
#: modules/custom/wt_priceformatter/src/Form/PriceFormatterSettings.php:73
msgid "Text after of the price"
msgstr "Text nach dem Preis"

#: modules/custom/wt_priceformatter/config/schema/wt_priceformatter.schema.yml:0
#: modules/custom/wt_priceformatter/src/Form/PriceFormatterSettings.php:80
msgid "Currency symbol"
msgstr "Währungs-Symbol"

#: modules/custom/wt_priceformatter/config/schema/wt_priceformatter.schema.yml:0
#: modules/custom/wt_priceformatter/src/Form/PriceFormatterSettings.php:86
msgid "Currency position"
msgstr "Position Währung"

#: modules/custom/wt_priceformatter/config/schema/wt_priceformatter.schema.yml:0
#: modules/custom/wt_priceformatter/src/Form/PriceFormatterSettings.php:99
msgid "Show all zero decimal places"
msgstr "Zeige Nachkommastellen wenn nur Nullen"

#: modules/custom/wt_priceformatter/config/schema/wt_priceformatter.schema.yml:0
#: modules/custom/wt_priceformatter/src/Form/PriceFormatterSettings.php:106
msgid "Style all zero decimal places as"
msgstr "Formatiere nur aus Nullen bestehende Nachkommastellen als"

#: modules/custom/wt_priceformatter/src/Form/PriceFormatterSettings.php:53
msgid ""
"Round fraction prices +- this value to the nearest integer, even when "
"<i>Number of decimal places</i> is greater than zero.<br>Use this to prevent "
"unwanted fraction prices due to division rounding.<br>E.g. set this value to "
"0.03 and a prices like 99.99 or 600.03 will be to rounded to 100.00 "
"respectively 600.00"
msgstr ""
"Runde Kommazahlen +- diesen Wert auf die nächste Ganzzahl, auch wenn "
"<i>Anzahl Nachkommastellen</i> größer als 0 ist.<br>Nützlich um ungewollte "
"Kommapreise wegen Rundungsfehlern aus Divisionen vorzubeugen.<br>Beispiel: "
"Wenn dieser Wert auf 0,03 gesetzt werden, werden Preise wie 99,99 oder "
"600,03 auf 100,00 btw. 600,00 gerundet."

#: modules/custom/wt_priceformatter/src/Form/PriceFormatterSettings.php:90
msgid "in front of the number"
msgstr "vor der Zahl"

#: modules/custom/wt_priceformatter/src/Form/PriceFormatterSettings.php:91
msgid "in the middle, before the decimal places (instead of decimal symbol)"
msgstr "in der Mitte, vor den Nachkommastellen (anstelle des Dezimal-Symbols)"

#: modules/custom/wt_priceformatter/src/Form/PriceFormatterSettings.php:92
msgid "after the number"
msgstr "nach der Zahl"

#: modules/custom/wt_priceformatter/src/Form/PriceFormatterSettings.php:101;113
msgid ""
"This setting only affects integer prices (after rounding to <i>Number of "
"decimal places</i>)"
msgstr ""
"Diese Einstellung betrifft nur ganzzahlige Preise (nach der Rundung auf "
"<i>Anzahl Nachkommastellen</i>)"

#: modules/custom/wt_priceformatter/src/Form/PriceFormatterSettings.php:110
msgid "zeros"
msgstr "Nullen"

#: modules/custom/wt_priceformatter/src/Form/PriceFormatterSettings.php:111
msgid "long dash"
msgstr "Geviert-Strich"

#: modules/custom/wt_priceformatter/src/Form/PriceFormatterSettings.php:146
msgid "The configuration options have been saved."
msgstr "Die Einstellungen wurden gespeichert."

#: modules/custom/wt_priceformatter/config/install/wt_priceformatter.settings.yml:0
msgid "."
msgstr ","

#: modules/custom/wt_priceformatter/config/install/wt_priceformatter.settings.yml:0
msgid "from "
msgstr "ab "

#: modules/custom/wt_priceformatter/config/install/wt_priceformatter.settings.yml:0
msgid "before"
msgstr "before"
