<?php

namespace Drupal\wt_priceformatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\wt_priceformatter\TwigExtension;

/**
 * Plugin implementation of the 'wt_priceformatter' formatter.
 *
 * @FieldFormatter(
 *   id = "wt_priceformatter",
 *   label = @Translation("Price with heavy HTML markup for CSS styling"),
 *   field_types = {
 *     "integer",
 *     "decimal",
 *     "float"
 *   }
 * )
 */
class PriceFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $config = \Drupal::config('wt_priceformatter.settings');
    return [
      'round' => $config->get('round'),
      'integer_rounding' => $config->get('integer_rounding'),
      'decimal_symbol' => $config->get('decimal_symbol'),
      'prefix' => $config->get('prefix'),
      'postfix' => $config->get('postfix'),
      'currency_symbol' => $config->get('currency_symbol'),
      'currency_position' => $config->get('currency_position'),
      'show_zeros' => !empty($config->get('show_zeros')),
      'zero_style' => $config->get('zero_style'),
      'css_classes' => '',
    ] + parent::defaultSettings();
  }


  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['round'] = array(
      '#type' => 'number',
      '#min' => 0,
      '#max' => 9,
      '#step' => 1,
      '#required' => true,
      '#title' => $this->t('Number of decimal places'),
      '#default_value' => $this->getSetting('round'),
    );

    $form['integer_rounding'] = array(
      '#type' => 'number',
      '#min' => 0,
      '#max' => 0.5,
      '#step' => 'any',
      '#required' => true,
      '#title' => $this->t('Additional integer rounding'),
      '#default_value' => $this->getSetting('integer_rounding'),
      '#description' => $this->t('Round fraction prices +- this value to the nearest integer, even when <i>Number of decimal places</i> is greater than zero.<br>Use this to prevent unwanted fraction prices due to division rounding.<br>E.g. set this value to 0.03 and a prices like 99.99 or 600.03 will be to rounded to 100.00 respectively 600.00'),
    );

    $form['decimal_symbol'] = array(
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#title' => $this->t('Decimal symbol'),
      '#default_value' => $this->getSetting('decimal_symbol'),
    );

    $form['prefix'] = array(
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#title' => $this->t('Text in front of the price'),
      '#default_value' => $this->getSetting('prefix'),
      '#description' => $this->t('On multi-lingual sites this string will be translated and should be entered in english.'),
    );

    $form['postfix'] = array(
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#title' => $this->t('Text after of the price'),
      '#default_value' => $this->getSetting('postfix'),
      '#description' => $this->t('On multi-lingual sites this string will be translated and should be entered in english.'),
    );

    $form['currency_symbol'] = array(
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#title' => $this->t('Currency symbol'),
      '#default_value' => $this->getSetting('currency_symbol'),
      '#description' => $this->t('On multi-lingual sites this string will be translated and should be entered in english.'),
    );

    $form['currency_position'] = [
      '#type' => 'radios',
      '#title' => $this->t('Currency position'),
      '#required' => true,
      '#default_value' => $this->getSetting('currency_position'),
      '#options' => [
        'before' => $this->t('in front of the number'),
        'middle' => $this->t('in the middle, before the decimal places (instead of decimal symbol)'),
        'after' => $this->t('after the number'),
      ],
      '#required' => true,
    ];

    $form['show_zeros'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show all zero decimal places'),
      '#default_value' => !empty($this->getSetting('show_zeros')),
      '#description' => $this->t('This setting only affects integer prices (after rounding to <i>Number of decimal places</i>)'),
    ];

    $form['zero_style'] = [
      '#type' => 'radios',
      '#title' => $this->t('Style all zero decimal places as'),
      '#required' => true,
      '#default_value' => $this->getSetting('zero_style'),
      '#options' => [
        'zeros' => $this->t('zeros'),
        'dash' => $this->t('long dash'),
      ],
      '#description' => $this->t('This setting only affects integer prices (after rounding to <i>Number of decimal places</i>)'),
    ];

    $form['css_classes'] = array(
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#title' => $this->t('CSS classes'),
      '#default_value' => $this->getSetting('css_classes'),
      '#description' => $this->t('Injected as string in the wrapping span\'s class attribute'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $settings = $this->getSettings();
    foreach (['prefix', 'postfix', 'decimal_symbol'] as $translatableSetting) {
      if ($settings[$translatableSetting]) {
        $settings[$translatableSetting] = t($settings[$translatableSetting]);
      }
    }

    foreach ($items as $delta => $item) {
      $twigFormatter = \Drupal::service('wt_priceformatter.twig.TwigExtension');
      $markup = $twigFormatter->price_format($item->value, $settings);
      $elements[$delta] = ['#markup' => $markup];
    }

    return $elements;
  }
}