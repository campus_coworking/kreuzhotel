<?php

namespace Drupal\wt_priceformatter\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class PriceFormatterSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'wt_priceformatter.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wt_priceformatter_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('wt_priceformatter.settings');

    $form['round'] = array(
      '#type' => 'number',
      '#min' => 0,
      '#max' => 9,
      '#step' => 1,
      '#required' => true,
      '#title' => $this->t('Number of decimal places'),
      '#default_value' => $config->get('round'),
    );

    $form['integer_rounding'] = array(
      '#type' => 'number',
      '#min' => 0,
      '#max' => 0.5,
      '#step' => 'any',
      '#required' => true,
      '#title' => $this->t('Additional integer rounding'),
      '#default_value' => $config->get('integer_rounding'),
      '#description' => $this->t('Round fraction prices +- this value to the nearest integer, even when <i>Number of decimal places</i> is greater than zero.<br>Use this to prevent unwanted fraction prices due to division rounding.<br>E.g. set this value to 0.03 and a prices like 99.99 or 600.03 will be to rounded to 100.00 respectively 600.00'),
    );

    $form['decimal_symbol'] = array(
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#title' => $this->t('Decimal symbol'),
      '#default_value' => $config->get('decimal_symbol'),
    );

    $form['prefix'] = array(
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#title' => $this->t('Text in front of the price'),
      '#default_value' => $config->get('prefix'),
    );

    $form['postfix'] = array(
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#title' => $this->t('Text after of the price'),
      '#default_value' => $config->get('postfix'),
    );

    $form['currency_symbol'] = array(
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#title' => $this->t('Currency symbol'),
      '#default_value' => $config->get('currency_symbol'),
    );

    $form['currency_position'] = [
      '#type' => 'radios',
      '#title' => $this->t('Currency position'),
      '#required' => true,
      '#default_value' => $config->get('currency_position'),
      '#options' => [
        'before' => $this->t('in front of the number'),
        'middle' => $this->t('in the middle, before the decimal places (instead of decimal symbol)'),
        'after' => $this->t('after the number'),
      ],
      '#required' => true,
    ];

    $form['show_zeros'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show all zero decimal places'),
      '#default_value' => !empty($config->get('show_zeros')),
      '#description' => $this->t('This setting only affects integer prices (after rounding to <i>Number of decimal places</i>)'),
    ];

    $form['zero_style'] = [
      '#type' => 'radios',
      '#title' => $this->t('Style all zero decimal places as'),
      '#required' => true,
      '#default_value' => $config->get('zero_style'),
      '#options' => [
        'zeros' => $this->t('zeros'),
        'dash' => $this->t('long dash'),
      ],
      '#description' => $this->t('This setting only affects integer prices (after rounding to <i>Number of decimal places</i>)'),
    ];


    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Config\Config $config */
    $config = \Drupal::service('config.factory')->getEditable('wt_priceformatter.settings');
    $config->set('round', $form_state->getValue('round'));
    $config->set('integer_rounding', $form_state->getValue('integer_rounding'));
    $config->set('decimal_symbol', $form_state->getValue('decimal_symbol'));
    $config->set('prefix', $form_state->getValue('prefix'));
    $config->set('postfix', $form_state->getValue('postfix'));
    $config->set('currency_symbol', $form_state->getValue('currency_symbol'));
    $config->set('currency_position', $form_state->getValue('currency_position'));
    $config->set('show_zeros', $form_state->getValue('show_zeros') == 1 ? true: false);
    $config->set('zero_style', $form_state->getValue('zero_style'));
    
    parent::submitForm($form, $form_state);

    $config->save();
    drupal_set_message($this->t('The configuration options have been saved.'));
  }

}
