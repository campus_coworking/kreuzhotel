<?php
namespace Drupal\wt_priceformatter\TwigExtension;

use Drupal\Core\Config\ConfigFactory;

/**
 * Class DefaultService.
 *
 * @package Drupal\TextColorizer
 */
class PriceFormatter extends \Twig_Extension {

  protected $config;

  public function __construct(ConfigFactory $config_factory) {
    $this->config = $config_factory->get('wt_priceformatter.settings');
  }
  
  /**
   * {@inheritdoc}
   * This function must return the name of the extension. It must be unique.
   */
  public function getName() {
    return 'wt_priceformatter.twig_extension';
  }

  /**
   * Generates a list of all Twig filters that this extension defines.
   */
  public function getFilters() {
    return [
      new \Twig_SimpleFilter('price_format', [$this, 'price_format'], ['is_safe' => ['html']]),
    ];
  }

  /**
   * Filter to return colorized text
   */
  public function price_format($number, $parameters = []) {
    $markup = $number;
    $allParameters = [
      'round', 
      'integer_rounding', 
      'decimal_symbol', 
      'prefix', 
      'postfix', 
      'currency_symbol', 
      'currency_position', 
      'show_zeros', 
      'zero_style',
      'css_classes',
    ];
    foreach ($allParameters as $paramName) {
      if (!array_key_exists($paramName, $parameters)) {
        $parameters[$paramName] = $this->config->get($paramName);
      }
    }
    if (is_numeric($number)) {
      $markup = '';
      $number = round($number, $parameters['round']);
      $integer = floor($number);
      $fraction = $number - $integer;
      if (
        $parameters['round'] > 0 && 
        $parameters['integer_rounding'] > 0 && 
        round($number) - $parameters['integer_rounding'] <= $number && 
        $number <= round($number) + $parameters['integer_rounding']
      ) {
        $number = $integer = round($number);
        $fraction = 0;
      }
      $fraction_digits = round($fraction * pow(10, $parameters['round']));
      $markup .= '<span class="' . $parameters['css_classes'] . ' price price--' . ($fraction > 0 ? 'fraction' : 'whole') . '">';
      
      if ($parameters['prefix']) {
        $markup .= '<span class="price__prefix">' . $parameters['prefix'] . '</span>';
      }
        
      if ($parameters['currency_symbol'] && $parameters['currency_position'] == 'before') {
        $markup .= '<span class="price__currency">' . $parameters['currency_symbol'] . '</span>';
      }
      
      $markup .= '<span class="price__whole">' . $integer . '</span>';
      
      if ($parameters['currency_symbol'] && $parameters['currency_position'] == 'middle') {
        $markup .= '<span class="price__currency">' . $parameters['currency_symbol'] . '</span>';
      }
      elseif ($parameters['decimal_symbol'] && ($fraction > 0 || $parameters['show_zeros'])) {
        $markup .= '<span class="price__dot">' . $parameters['decimal_symbol'] . '</span>';
      }
      
      if ($fraction > 0) {
        $markup .= '<span class="price__fraction price__fraction--not-zero">' . str_pad($fraction_digits, $parameters['round'], '0') . '</span>';
      }
      elseif ($parameters['show_zeros'] && $fraction == 0) {
        if ($parameters['zero_style'] == 'zeros') {
          $markup .= '<span class="price__fraction price__fraction--zeros">' . str_pad('0', $parameters['round'], '0') . '</span>';
        }
        elseif ($parameters['zero_style'] == 'dash') {
          $markup .= '<span class="price__fraction price__fraction--dash">&ndash;</span>';
        }
      }

      if ($parameters['currency_symbol'] && $parameters['currency_position'] == 'after') {
        $markup .= '<span class="price__currency">' . $parameters['currency_symbol'] . '</span>';
      }
        
      if ($parameters['postfix']) {
        $markup .= '<span class="price__postfix">' . $parameters['postfix'] . '</span>';
      }
      $markup .= '</span>';
    }
    return $markup;
  }
}