// global configuration values
var wt = {
  colorboxTreshold: 767, // colorbox will only fire on screens above this width
  toolbarHeight: 0, // height of the Drupal admin toolbar (to prevent content being overlapped for editors)
  fixedTop: 0, // height of all fixed top UI components (e.g. to calculate additional offset for wt.scrollTo)
  mainMenu: {
    container: '.nav__ul--main-0', // selector string for container to toggle when viewport crosses treshold
    viewMode: false, // visual layout of main menu (xs = mobile/accordion, lg = desktop/dropdown)
    toggleBtn: '.nav__togglebtn--main', // selector string for button that handles expand/collapse when viewmode == xs
    treshold: 990, // main menu will only be lg on screens above this width in pixel
    type: 'horizontal' // 'horizontal' or 'vertical'
  },
  ready: [] // delayed ready handlers (so that they can be overwritten by child themes)
};
;
