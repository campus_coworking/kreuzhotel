#!/usr/bin/env sh

sass ./web/themes/custom/wtadmin_base/scss/toolbar.scss ./web/themes/custom/wtadmin_base/css/toolbar.css --style expanded --load-path ./web/themes/custom/wtfrontend_base/scss
sass ./web/themes/custom/wtadmin/scss/wtadmin.scss ./web/themes/custom/wtadmin/css/wtadmin.css --style expanded --load-path ./web/themes/custom/wtadmin_base/scss
sass ./web/themes/custom/wtfrontend/scss/wtfrontend.scss ./web/themes/custom/wtfrontend/css/wtfrontend.css --style expanded --load-path ./web/themes/custom/wtfrontend_base/scss
postcss --use autoprefixer --output ./web/themes/custom/wtadmin_base/css/toolbar.css ./web/themes/custom/wtadmin_base/css/toolbar.css
postcss --use autoprefixer --output ./web/themes/custom/wtadmin/css/wtadmin.css ./web/themes/custom/wtadmin/css/wtadmin.css
postcss --use autoprefixer --output ./web/themes/custom/wtfrontend/css/wtfrontend.css ./web/themes/custom/wtfrontend/css/wtfrontend.css
